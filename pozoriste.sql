-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 30, 2019 at 10:12 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pozoriste`
--

-- --------------------------------------------------------

--
-- Table structure for table `autor`
--

CREATE TABLE `autor` (
  `idAutor` int(11) NOT NULL,
  `imeAutor` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `autor`
--

INSERT INTO `autor` (`idAutor`, `imeAutor`) VALUES
(1, 'Djuzepe Verdi'),
(2, 'Goran Bregovic'),
(3, 'Branislav Nusic'),
(4, 'Djakomo Pucini'),
(5, 'Aleksandar Popovic'),
(6, 'Danijel Kish'),
(7, 'Maksim Gorki'),
(8, 'Ivo Andric'),
(9, 'Harold Pinter'),
(10, 'Ana Djordjevic'),
(11, 'Vladimir Djurdjevic'),
(12, 'Dusan Kovacevic'),
(13, 'Gaston Leru'),
(14, 'Vladimir Lazic'),
(15, 'Zan Batist Molijer'),
(16, 'Valentin Rasputin'),
(20, 'test'),
(21, 'test2'),
(22, 'test3');

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

CREATE TABLE `korisnik` (
  `idKorisnik` int(11) NOT NULL,
  `imeKorisnik` varchar(30) NOT NULL,
  `prezimeKorisnik` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `emailKorisnik` varchar(30) NOT NULL,
  `telefonKorisnik` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `korisnik`
--

INSERT INTO `korisnik` (`idKorisnik`, `imeKorisnik`, `prezimeKorisnik`, `username`, `password`, `emailKorisnik`, `telefonKorisnik`) VALUES
(1, 'Test', 'Testprezime', 'test', 'test', 'test@test.test', '0652222222'),
(2, 'Test2', 'Test2', 'tt', 'tt', 't@t.t', '1212121212'),
(3, 'Administrator', 'Administrator', 'admin', 'admin', 'admin@pozorista.com', '0652015805');

-- --------------------------------------------------------

--
-- Table structure for table `korisnikrezervacija`
--

CREATE TABLE `korisnikrezervacija` (
  `idkorisnikRezervacija` int(11) NOT NULL,
  `idKorisnik` int(11) NOT NULL,
  `idPredstava` int(11) NOT NULL,
  `kolicinaKarte` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `korisnikrezervacija`
--

INSERT INTO `korisnikrezervacija` (`idkorisnikRezervacija`, `idKorisnik`, `idPredstava`, `kolicinaKarte`) VALUES
(3, 1, 1, 2),
(4, 2, 1, 10),
(7, 1, 11, 4),
(9, 1, 12, 4),
(11, 1, 9, 4),
(15, 1, 15, 2),
(16, 1, 8, 30),
(18, 1, 13, 6),
(19, 1, 22, 10),
(20, 1, 24, 10),
(23, 3, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `pozoriste`
--

CREATE TABLE `pozoriste` (
  `idPozoriste` int(11) NOT NULL,
  `nazivPozoriste` varchar(50) NOT NULL,
  `adresaPozoriste` varchar(50) NOT NULL,
  `kapacitet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pozoriste`
--

INSERT INTO `pozoriste` (`idPozoriste`, `nazivPozoriste`, `adresaPozoriste`, `kapacitet`) VALUES
(1, 'Narodno pozoriste', 'Trg Republike 1', 800),
(2, 'Jugoslovensko dramsko pozoriste', 'Kneza Milana 50', 500),
(3, 'Beogradsko dramsko pozoriste', 'Milesevska 64b', 500),
(4, 'Atelje 212', 'Svetogorska 21', 300),
(5, 'Pozoriste na Terazijama', 'Terazije 29', 500),
(6, 'Zvezdara teatar', 'Milana Rakica 38', 350);

-- --------------------------------------------------------

--
-- Table structure for table `predstava`
--

CREATE TABLE `predstava` (
  `idPredstava` int(11) NOT NULL,
  `idPozoriste` int(11) NOT NULL,
  `nazivPredstava` varchar(50) NOT NULL,
  `datumPredstava` date NOT NULL,
  `vremePredstava` varchar(5) NOT NULL,
  `cenakarte` double NOT NULL,
  `idAutor` int(11) NOT NULL,
  `idZanr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `predstava`
--

INSERT INTO `predstava` (`idPredstava`, `idPozoriste`, `nazivPredstava`, `datumPredstava`, `vremePredstava`, `cenakarte`, `idAutor`, `idZanr`) VALUES
(1, 1, 'Gospodja ministarka', '2021-02-03', '19:00', 500, 3, 2),
(2, 1, 'Ozaloscena porodica', '2021-02-09', '19:30', 500, 3, 2),
(3, 1, 'Trubadur', '2021-02-24', '19:30', 1000, 1, 4),
(4, 1, 'Kraljica Margo', '2021-02-20', '19:30', 300, 2, 3),
(5, 1, 'Madam Baterflaj', '2021-02-14', '19:00', 800, 4, 4),
(6, 1, 'Bal pod maskama', '2021-02-28', '19:30', 500, 1, 4),
(7, 2, 'Izdaja', '2021-02-12', '20:00', 500, 9, 1),
(8, 2, 'Svabica', '2021-02-25', '20:00', 500, 10, 1),
(9, 2, 'Tako je moralo biti', '2021-02-27', '20:00', 600, 3, 2),
(10, 2, 'Uobrazeni Bolesnik', '2021-02-19', '20:00', 600, 15, 1),
(11, 2, 'Gospodjica', '2021-02-13', '20:00', 500, 8, 1),
(12, 3, 'Palilulski romani', '2021-02-09', '20:00', 600, 3, 1),
(13, 3, 'Tri klase i godpodja Nusic', '2021-02-12', '20:00', 500, 11, 1),
(14, 3, 'Poslednji rok, premijera', '2021-02-20', '20:00', 1000, 16, 1),
(15, 3, 'Poslednji rok', '2021-02-22', '20:00', 800, 16, 1),
(16, 4, 'Urnebesna tragedija', '2021-02-04', '20:00', 500, 12, 2),
(17, 4, 'Urnebesna tragedija', '2021-02-22', '20:00', 500, 12, 2),
(18, 4, 'Urnebesna tragedija', '2021-02-23', '20:00', 500, 12, 2),
(19, 4, 'Mrescenje sarana', '2021-02-20', '20:00', 500, 5, 2),
(20, 4, 'Mrescenje sarana', '2021-02-26', '20:00', 500, 5, 2),
(21, 4, 'Urnebesna tragedija', '2021-02-10', '20:00', 500, 12, 2),
(22, 5, 'Fantom iz opere', '2021-02-13', '19:30', 800, 13, 4),
(23, 5, 'Fantom iz opere', '2021-02-14', '19:30', 800, 13, 4),
(24, 5, 'Fantom iz opere', '2021-02-24', '19:30', 800, 13, 4),
(26, 5, 'Cigani lete u nebo', '2021-02-22', '19:30', 600, 14, 5),
(28, 1, 'Elektra', '2021-02-20', '20:30', 800, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `zanr`
--

CREATE TABLE `zanr` (
  `idZanr` int(11) NOT NULL,
  `nazivZanr` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zanr`
--

INSERT INTO `zanr` (`idZanr`, `nazivZanr`) VALUES
(1, 'Drama'),
(2, 'Komedija'),
(3, 'Balet'),
(4, 'Opera'),
(5, 'Mjuzikl');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `autor`
--
ALTER TABLE `autor`
  ADD PRIMARY KEY (`idAutor`);

--
-- Indexes for table `korisnik`
--
ALTER TABLE `korisnik`
  ADD PRIMARY KEY (`idKorisnik`);

--
-- Indexes for table `korisnikrezervacija`
--
ALTER TABLE `korisnikrezervacija`
  ADD PRIMARY KEY (`idkorisnikRezervacija`),
  ADD KEY `idKorisnik` (`idKorisnik`),
  ADD KEY `idPredstava` (`idPredstava`);

--
-- Indexes for table `pozoriste`
--
ALTER TABLE `pozoriste`
  ADD PRIMARY KEY (`idPozoriste`);

--
-- Indexes for table `predstava`
--
ALTER TABLE `predstava`
  ADD PRIMARY KEY (`idPredstava`),
  ADD KEY `idPozoriste` (`idPozoriste`),
  ADD KEY `idZanr` (`idZanr`),
  ADD KEY `idAutor` (`idAutor`);

--
-- Indexes for table `zanr`
--
ALTER TABLE `zanr`
  ADD PRIMARY KEY (`idZanr`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `autor`
--
ALTER TABLE `autor`
  MODIFY `idAutor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `korisnik`
--
ALTER TABLE `korisnik`
  MODIFY `idKorisnik` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `korisnikrezervacija`
--
ALTER TABLE `korisnikrezervacija`
  MODIFY `idkorisnikRezervacija` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `pozoriste`
--
ALTER TABLE `pozoriste`
  MODIFY `idPozoriste` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `predstava`
--
ALTER TABLE `predstava`
  MODIFY `idPredstava` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `zanr`
--
ALTER TABLE `zanr`
  MODIFY `idZanr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `korisnikrezervacija`
--
ALTER TABLE `korisnikrezervacija`
  ADD CONSTRAINT `korisnikrezervacija_ibfk_1` FOREIGN KEY (`idKorisnik`) REFERENCES `korisnik` (`idKorisnik`),
  ADD CONSTRAINT `korisnikrezervacija_ibfk_2` FOREIGN KEY (`idPredstava`) REFERENCES `predstava` (`idPredstava`);

--
-- Constraints for table `predstava`
--
ALTER TABLE `predstava`
  ADD CONSTRAINT `predstava_ibfk_1` FOREIGN KEY (`idPozoriste`) REFERENCES `pozoriste` (`idPozoriste`),
  ADD CONSTRAINT `predstava_ibfk_2` FOREIGN KEY (`idZanr`) REFERENCES `zanr` (`idZanr`),
  ADD CONSTRAINT `predstava_ibfk_3` FOREIGN KEY (`idAutor`) REFERENCES `autor` (`idAutor`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
