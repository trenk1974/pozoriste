package broker;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import domen.Autor;
import domen.Korisnik;
import domen.Pozoriste;
import domen.Predstava;
import domen.RezervacijaKorisnik;
import domen.RezervacijePredstava;
import domen.Zanr;

public class DBComm {

	public static DBComm broker;
	private Connection con;

	private DBComm() {
		ucitajDriver();
	}

	public static DBComm getBroker() {
		if (broker == null) {
			broker = new DBComm();
		}
		return broker;
	}

	private void ucitajDriver() {

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void otvoriKonekciju() {

		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost/pozoriste", "root", "");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void zatvoriKonekciju() {

		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public boolean proveriUsername(String username) {
		boolean postoji = false;
		String sql = "Select * from korisnik where username='" + username + "'";
		Statement st = null;
		ResultSet rs = null;
		try {
			st = con.createStatement();
			rs = st.executeQuery(sql);
			if (rs.next()) {
				postoji = true;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return postoji;
	}

	public void unesiKorisnik(String imeKorisnik, String prezimeKorisnik, String username, String password1,
			String emailKorisnik, String telefonKorisnik) {
		String sql = "INSERT INTO korisnik(imeKorisnik, prezimeKorisnik, username, password, emailKorisnik, telefonKorisnik)VALUES(?,?,?,?,?,?)";

		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, imeKorisnik);
			ps.setString(2, prezimeKorisnik);
			ps.setString(3, username);
			ps.setString(4, password1);
			ps.setString(5, emailKorisnik);
			ps.setString(6, telefonKorisnik);
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean proveriPass(String username, String password) {

		String sql = "Select password from korisnik WHERE username='" + username + "'";
		ResultSet rs = null;
		Statement st = null;
		String pass = null;

		try {
			st = con.createStatement();
			rs = st.executeQuery(sql);
			while (rs.next()) {
				pass = rs.getString("password");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (pass.equals(password)) {
			return true;
		}

		return false;
	}

	public Korisnik vratiKorisnik(String user) {
		String sql = "Select * FROM korisnik WHERE username='" + user + "'";
		ResultSet rs = null;
		Statement st = null;
		Korisnik k = new Korisnik();

		try {
			st = con.createStatement();
			rs = st.executeQuery(sql);
			while (rs.next()) {
				k.setIdKorisnik(rs.getInt("idKorisnik"));
				k.setIme(rs.getString("imeKorisnik"));
				k.setPrezime(rs.getString("prezimeKorisnik"));
				k.setUsername(rs.getString("username"));
				k.setPassword(rs.getString("password"));
				k.setEmail(rs.getString("emailKorisnik"));
				k.setTelefon(rs.getString("telefonKorisnik"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return k;
	}

	public ArrayList<Predstava> vratiPredstave(String datumFormatiran) {

		ArrayList<Predstava> alPredstave = new ArrayList<>();

		String sql = "SELECT * FROM `predstava` "
				+ "INNER JOIN pozoriste ON pozoriste.idPozoriste=predstava.idPozoriste "
				+ "INNER join autor ON autor.idAutor=predstava.idAutor "
				+ "INNER join zanr ON zanr.idZanr=predstava.idZanr" + " WHERE predstava.datumPredstava>'"
				+ datumFormatiran + "' ORDER BY nazivPredstava";
		System.out.println(sql);

		ResultSet rs = null;
		Statement st = null;

		try {
			st = con.createStatement();
			rs = st.executeQuery(sql);
			while (rs.next()) {
				Predstava p = new Predstava();
				p.setIdPredstava(rs.getInt("idPredstava"));
				p.setNazivPredstava(rs.getString("nazivPredstava"));
				p.setNazivPozoriste(rs.getString("nazivPozoriste"));
				p.setDatumPredstave(rs.getDate("datumPredstava"));
				p.setVremePrestava(rs.getString("vremePredstava"));
				p.setCena(rs.getDouble("cenakarte"));
				p.setAutor(rs.getString("imeAutor"));
				p.setZanr(rs.getString("nazivZanr"));
				p.setKapacitet(rs.getInt("kapacitet"));
				alPredstave.add(p);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return alPredstave;
	}

	public ArrayList<RezervacijePredstava> vratiRezervacijePredstava(String datumFormatiran) {

		ArrayList<RezervacijePredstava> alRezervacijePredstava = new ArrayList<>();

		String sql = "SELECT *, sum(korisnikrezervacija.kolicinaKarte) as karte FROM `predstava` \r\n"
				+ "INNER join korisnikrezervacija on korisnikrezervacija.idPredstava=predstava.idPredstava\r\n"
				+ "WHERE predstava.datumPredstava>'" + datumFormatiran + "'\r\n"
				+ "GROUP by korisnikrezervacija.idPredstava";
		ResultSet rs = null;
		Statement st = null;

		try {
			st = con.createStatement();
			rs = st.executeQuery(sql);
			while (rs.next()) {
				RezervacijePredstava rp = new RezervacijePredstava();
				rp.setIdRezervacija(rs.getInt("idkorisnikRezervacija"));
				rp.setIdKorisnik(rs.getInt("idKorisnik"));
				rp.setIdPredstava(rs.getInt("idPredstava"));
				rp.setKolicinaKarte(rs.getInt("karte"));
				alRezervacijePredstava.add(rp);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return alRezervacijePredstava;
	}

	public ArrayList<Pozoriste> vratiPozoriste() {
		String sql = "Select * from pozoriste order by nazivPozoriste";
		ArrayList<Pozoriste> alPozoriste = new ArrayList<>();
		ResultSet rs = null;
		Statement st = null;

		try {
			st = con.createStatement();
			rs = st.executeQuery(sql);
			while (rs.next()) {
				Pozoriste p = new Pozoriste();
				p.setIdPozoriste(rs.getInt("idPozoriste"));
				p.setNazivPozoriste(rs.getString("nazivPozoriste"));
				p.setAdresaPozoriste(rs.getString("adresaPozoriste"));
				p.setKapacitet(rs.getInt("kapacitet"));
				alPozoriste.add(p);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return alPozoriste;
	}

	public ArrayList<RezervacijaKorisnik> vratiRezervacijeKorisnik(int idKorisnik, String datumFormatiran) {
		ArrayList<RezervacijaKorisnik> alRezervacijeKorisnik = new ArrayList<>();
		String sql = "SELECT * FROM `predstava`\r\n"
				+ "INNER join korisnikrezervacija ON korisnikrezervacija.idPredstava=predstava.idPredstava\r\n"
				+ "INNER join korisnik on korisnikrezervacija.idKorisnik=korisnik.idKorisnik\r\n"
				+ "INNER JOIN pozoriste on pozoriste.idPozoriste=predstava.idPozoriste\r\n"
				+ "WHERE korisnikrezervacija.idKorisnik=" + idKorisnik + " AND predstava.datumPredstava>'"
				+ datumFormatiran + "'";
		System.out.println(sql);
		ResultSet rs = null;
		Statement st = null;

		try {
			st = con.createStatement();
			rs = st.executeQuery(sql);
			while (rs.next()) {
				RezervacijaKorisnik rk = new RezervacijaKorisnik();
				rk.setIdPredstava(rs.getInt("idPredstava"));
				rk.setNazivPredstava(rs.getString("nazivPredstava"));
				rk.setNazivPozoriste(rs.getString("nazivPozoriste"));
				rk.setDatumPredstave(rs.getDate("datumPredstava"));
				rk.setRezervisanoKarata(rs.getInt("kolicinaKarte"));
				rk.setCenaKarte(rs.getDouble("cenakarte"));
				rk.setIdKorisnik(idKorisnik);
				rk.setUsername(rs.getString("username"));
				rk.setIdRezervacije(rs.getInt("idkorisnikRezervacija"));
				alRezervacijeKorisnik.add(rk);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return alRezervacijeKorisnik;

	}

	public void obrisiRezervaciju(int idRez) {
		String sql = "DELETE FROM korisnikrezervacija WHERE idkorisnikRezervacija=" + idRez;

		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void promeniRezervaciju(int idRez, int novaKolicina) {
		String sql = "UPDATE korisnikrezervacija SET kolicinaKarte=" + novaKolicina + " WHERE idkorisnikRezervacija="
				+ idRez;

		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void unesiRezervaciju(int idPredstave, int idKorisnik, int novaKolicina) {
		String sqlprovera = "SELECT * FROM korisnikrezervacija WHERE idKorisnik=" + idKorisnik + " AND idPredstava="
				+ idPredstave;
		Statement st = null;
		ResultSet rs = null;
		int idRezervacije = 0;
		int prijasnjaKolicina = 0;

		try {
			st = con.createStatement();
			rs = st.executeQuery(sqlprovera);
			while (rs.next()) {
				idRezervacije = rs.getInt("idkorisnikRezervacija");
				prijasnjaKolicina = rs.getInt("kolicinaKarte");
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (idRezervacije == 0) {
			String sql = "INSERT INTO korisnikrezervacija (idKorisnik, idPredstava, kolicinaKarte)" + " VALUES (?,?,?)";
			try {
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setInt(1, idKorisnik);
				ps.setInt(2, idPredstave);
				ps.setInt(3, novaKolicina);
				ps.execute();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			novaKolicina = novaKolicina + prijasnjaKolicina;
			promeniRezervaciju(idRezervacije, novaKolicina);
		}

	}

	public void izmeniPozoriste(Pozoriste izmeniPozoriste) {
		int id=izmeniPozoriste.getIdPozoriste();
		String naziv=izmeniPozoriste.getNazivPozoriste();
		String adresa=izmeniPozoriste.getAdresaPozoriste();
		int kapacitet=izmeniPozoriste.getKapacitet();
		
		String sql="UPDATE pozoriste SET nazivPozoriste='"+naziv+"', adresaPozoriste='"+adresa+"', kapacitet="+kapacitet+" WHERE idPozoriste="+id;
		
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void unesiPozoriste(Pozoriste novoPozoriste) {
		String sql = "INSERT INTO pozoriste (nazivPozoriste, adresaPozoriste, kapacitet)" + " VALUES (?,?,?)";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, novoPozoriste.getNazivPozoriste());
			ps.setString(2, novoPozoriste.getAdresaPozoriste());
			ps.setInt(3, novoPozoriste.getKapacitet());
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public ArrayList<Autor> vratiAutor() {
		String sql = "Select * from autor order by imeAutor";
		ArrayList<Autor> alAutor = new ArrayList<>();
		ResultSet rs = null;
		Statement st = null;

		try {
			st = con.createStatement();
			rs = st.executeQuery(sql);
			while (rs.next()) {
				Autor a = new Autor();
				a.setIdAutor(rs.getInt("idAutor"));
				a.setImeAutor(rs.getString("imeAutor"));
				alAutor.add(a);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return alAutor;
	}
	public ArrayList<Zanr> vratiZanr() {
		String sql = "Select * from zanr order by nazivZanr";
		ArrayList<Zanr> alZanr = new ArrayList<>();
		ResultSet rs = null;
		Statement st = null;

		try {
			st = con.createStatement();
			rs = st.executeQuery(sql);
			while (rs.next()) {
				Zanr z = new Zanr();
				z.setIdZanr(rs.getInt("idZanr"));
				z.setZanr(rs.getString("nazivZanr"));
				alZanr.add(z);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return alZanr;
	}

	public void obrisiPredstavu(int idPredstave) {
		String sql = "DELETE FROM predstava WHERE idPredstava=" + idPredstave;
		boolean obrisano=false;
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.execute();
			obrisano=true;
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Greska prilikom brisanja, Vec postoje rezervacije za predstavu!");
			e.printStackTrace();
		}
		if(obrisano) {
			JOptionPane.showMessageDialog(null, "Predstava izbrisana iz repertoara!");
		}
	}

	public void unesiPredstavu(Predstava p) {
		int idPozoriste=0;
		int idZanr=0;
		int idAutor=0;
		String sqlIDPozoriste="SELECT idPozoriste FROM pozoriste WHERE nazivPozoriste='"+p.getNazivPozoriste()+"' ";
		String sqlIDAutor="SELECT idAutor FROM autor WHERE imeAutor='"+p.getAutor()+"' ";
		String sqlIDZanr="SELECT idZanr FROM zanr WHERE nazivZanr='"+p.getZanr()+"' ";
		String sql="INSERT INTO predstava (idPozoriste, nazivPredstava, datumPredstava, vremePredstava, cenakarte,"
				+ " idAutor, idZanr) VALUES (?,?,?,?,?,?,?)";
		Statement stPozoriste=null;
		Statement stAutor=null;
		Statement stZanr=null;
				
		ResultSet rsPozoriste=null;
		ResultSet rsAutor=null;
		ResultSet rsZanr=null;
		
		try {
			con.setAutoCommit(false);
			
			stPozoriste=con.createStatement();
			rsPozoriste=stPozoriste.executeQuery(sqlIDPozoriste);
			stAutor=con.createStatement();
			rsAutor=stAutor.executeQuery(sqlIDAutor);
			stZanr=con.createStatement();
			rsZanr=stZanr.executeQuery(sqlIDZanr);
			while(rsPozoriste.next()) {
				idPozoriste=rsPozoriste.getInt("idPozoriste");
			}
			while(rsAutor.next()) {
				idAutor=rsAutor.getInt("idAutor");
			}
			while(rsZanr.next()) {
				idZanr=rsZanr.getInt("idZanr");
			}
			PreparedStatement ps=con.prepareStatement(sql);
			ps.setInt(1, idPozoriste);
			ps.setString(2, p.getNazivPredstava());
			ps.setString(3,p.getDatumString());
			ps.setString(4, p.getVremePrestava());
			ps.setDouble(5, p.getCena());
			ps.setInt(6, idAutor);
			ps.setInt(7, idZanr);
			ps.execute();
			
			
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, "Upis predstave neuspesan, SQL Error!");
			e.printStackTrace();
		}
		try {
			con.setAutoCommit(true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void unesiAutor(String autor) {
		String sql = "INSERT INTO autor (imeAutor) VALUES (?)";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, autor);
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void unesiZanr(String zanr) {
		String sql = "INSERT INTO zanr (nazivZanr) VALUES (?)";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, zanr);
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public boolean proveriSlobodnuKolicinu(int idPredstava, String nazivPozorista, int novaKol, int staraKolicina) {
		boolean slobodno=false;
		String sqlKapacitet="SELECT `kapacitet` FROM `pozoriste` WHERE nazivPozoriste='"+nazivPozorista+"'";
		String sqlRezervisano="SELECT SUM(kolicinaKarte) AS rezervisano FROM korisnikrezervacija WHERE idPredstava="+idPredstava;
		
		int kapacitet=0;
		int rezervisano=0;
		
		Statement stKap=null;
		Statement stRez=null;
		
		ResultSet rsKap=null;
		ResultSet rsRez=null;
		
		try {
			con.setAutoCommit(false);
			stKap=con.createStatement();
			rsKap=stKap.executeQuery(sqlKapacitet);
			stRez=con.createStatement();
			rsRez=stRez.executeQuery(sqlRezervisano);
			while(rsKap.next()) {
				kapacitet=rsKap.getInt("kapacitet");
			}
			while(rsRez.next()) {
				rezervisano=rsRez.getInt("rezervisano");
			}
			if(kapacitet>=((rezervisano-staraKolicina)+novaKol)) {
				slobodno=true;
			}
			
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		
		return slobodno;
	}

}
