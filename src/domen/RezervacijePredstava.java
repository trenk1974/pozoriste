package domen;

public class RezervacijePredstava {
	
	int idRezervacija;
	int idKorisnik;
	int idPredstava;
	int kolicinaKarte;
	public int getIdRezervacija() {
		return idRezervacija;
	}
	public void setIdRezervacija(int idRezervacija) {
		this.idRezervacija = idRezervacija;
	}
	public int getIdKorisnik() {
		return idKorisnik;
	}
	public void setIdKorisnik(int idKorisnik) {
		this.idKorisnik = idKorisnik;
	}
	public int getIdPredstava() {
		return idPredstava;
	}
	public void setIdPredstava(int idPredstava) {
		this.idPredstava = idPredstava;
	}
	public int getKolicinaKarte() {
		return kolicinaKarte;
	}
	public void setKolicinaKarte(int kolicinaKarte) {
		this.kolicinaKarte = kolicinaKarte;
	}
	

}
