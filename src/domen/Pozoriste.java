package domen;

public class Pozoriste {
	private String nazivPozoriste;
	private int idPozoriste;
	private String adresaPozoriste;
	private int kapacitet;
	public String getNazivPozoriste() {
		return nazivPozoriste;
	}
	public void setNazivPozoriste(String nazivPozoriste) {
		this.nazivPozoriste = nazivPozoriste;
	}
	public int getIdPozoriste() {
		return idPozoriste;
	}
	public void setIdPozoriste(int idPozoriste) {
		this.idPozoriste = idPozoriste;
	}
	public String getAdresaPozoriste() {
		return adresaPozoriste;
	}
	public void setAdresaPozoriste(String adresaPozoriste) {
		this.adresaPozoriste = adresaPozoriste;
	}
	public int getKapacitet() {
		return kapacitet;
	}
	public void setKapacitet(int kapacitet) {
		this.kapacitet = kapacitet;
	}
	

}
