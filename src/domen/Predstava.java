package domen;

import java.util.Date;

public class Predstava {
	int idPredstava;
	String nazivPozoriste;
	String nazivPredstava;
	Date datumPredstave;
	String vremePrestava;
	double cena;
	String autor;
	String zanr;
	int kapacitet;
	String datumString;
	public int getKapacitet() {
		return kapacitet;
	}
	public void setKapacitet(int kapacitet) {
		this.kapacitet = kapacitet;
	}

	public int getIdPredstava() {
		return idPredstava;
	}
	public void setIdPredstava(int idPredstava) {
		this.idPredstava = idPredstava;
	}
	public String getNazivPozoriste() {
		return nazivPozoriste;
	}
	public void setNazivPozoriste(String nazivPozoriste) {
		this.nazivPozoriste = nazivPozoriste;
	}
	public String getNazivPredstava() {
		return nazivPredstava;
	}
	public void setNazivPredstava(String nazivPredstava) {
		this.nazivPredstava = nazivPredstava;
	}
	public Date getDatumPredstave() {
		return datumPredstave;
	}
	public void setDatumPredstave(Date datumPredstave) {
		this.datumPredstave = datumPredstave;
	}
	public String getVremePrestava() {
		return vremePrestava;
	}
	public void setVremePrestava(String vremePrestava) {
		this.vremePrestava = vremePrestava;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getZanr() {
		return zanr;
	}
	public void setZanr(String zanr) {
		this.zanr = zanr;
	}

	public String getDatumString() {
		return datumString;
	}
	public void setDatumString(String datumString) {
		this.datumString = datumString;
	}

	
}
