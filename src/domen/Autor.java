package domen;

public class Autor {
	private int idAutor;
	private String imeAutor;
	public int getIdAutor() {
		return idAutor;
	}
	public void setIdAutor(int idAutor) {
		this.idAutor = idAutor;
	}
	public String getImeAutor() {
		return imeAutor;
	}
	public void setImeAutor(String imeAutor) {
		this.imeAutor = imeAutor;
	}

}
