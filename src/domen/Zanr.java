package domen;

public class Zanr {
	private int idZanr;
	private String zanr;
	public int getIdZanr() {
		return idZanr;
	}
	public void setIdZanr(int idZanr) {
		this.idZanr = idZanr;
	}
	public String getZanr() {
		return zanr;
	}
	public void setZanr(String zanr) {
		this.zanr = zanr;
	}

}
