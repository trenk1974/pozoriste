package domen;

import java.util.Date;

public class RezervacijaKorisnik {
	int idKorisnik;
	String username;
	int idPredstava;
	String nazivPredstava;
	String nazivPozoriste;
	Date datumPredstave;
	int rezervisanoKarata;
	double cenaKarte;
	int idRezervacije;
	
	public int getIdRezervacije() {
		return idRezervacije;
	}
	public void setIdRezervacije(int idRezervacije) {
		this.idRezervacije = idRezervacije;
	}
	public int getIdKorisnik() {
		return idKorisnik;
	}
	public void setIdKorisnik(int idKorisnik) {
		this.idKorisnik = idKorisnik;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getIdPredstava() {
		return idPredstava;
	}
	public void setIdPredstava(int idPredstava) {
		this.idPredstava = idPredstava;
	}
	public String getNazivPredstava() {
		return nazivPredstava;
	}
	public void setNazivPredstava(String nazivPredstava) {
		this.nazivPredstava = nazivPredstava;
	}
	public String getNazivPozoriste() {
		return nazivPozoriste;
	}
	public void setNazivPozoriste(String nazivPozoriste) {
		this.nazivPozoriste = nazivPozoriste;
	}
	public Date getDatumPredstave() {
		return datumPredstave;
	}
	public void setDatumPredstave(Date datumPredstave) {
		this.datumPredstave = datumPredstave;
	}
	public int getRezervisanoKarata() {
		return rezervisanoKarata;
	}
	public void setRezervisanoKarata(int rezervisanoKarata) {
		this.rezervisanoKarata = rezervisanoKarata;
	}
	public double getCenaKarte() {
		return cenaKarte;
	}
	public void setCenaKarte(double cenaKarte) {
		this.cenaKarte = cenaKarte;
	}
	

}
