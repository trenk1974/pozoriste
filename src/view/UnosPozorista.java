package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import domen.Pozoriste;
import kontroler.Kontroler;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class UnosPozorista extends JFrame {

	private JPanel contentPane;
	private JTextField tfNaziv;
	private JTextField tfAdresa;
	private JTextField tfKapacitet;
	private JTable table;
	private DefaultTableModel dtm = new DefaultTableModel();
	private JButton btnIzmeni;
	private ArrayList<Pozoriste> alPozoriste = new ArrayList<>();
	private int selektovaniRed;
	private int idPozoriste;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UnosPozorista frame = new UnosPozorista();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UnosPozorista() {
		setTitle("Unos pozorista");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 620, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Naziv pozorista:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(10, 11, 140, 14);
		contentPane.add(lblNewLabel);

		JLabel lblAdresaPozorista = new JLabel("Adresa pozorista:");
		lblAdresaPozorista.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAdresaPozorista.setBounds(10, 36, 140, 14);
		contentPane.add(lblAdresaPozorista);

		JLabel lblKapacitetPozorista = new JLabel("Kapacitet pozorista:");
		lblKapacitetPozorista.setHorizontalAlignment(SwingConstants.RIGHT);
		lblKapacitetPozorista.setBounds(10, 61, 140, 14);
		contentPane.add(lblKapacitetPozorista);

		tfNaziv = new JTextField();
		tfNaziv.setBounds(160, 8, 200, 20);
		contentPane.add(tfNaziv);
		tfNaziv.setColumns(10);

		tfAdresa = new JTextField();
		tfAdresa.setColumns(10);
		tfAdresa.setBounds(160, 33, 200, 20);
		contentPane.add(tfAdresa);

		tfKapacitet = new JTextField();
		tfKapacitet.setColumns(10);
		tfKapacitet.setBounds(160, 58, 100, 20);
		contentPane.add(tfKapacitet);

		JButton btnUnesi = new JButton("Unesi");
		btnUnesi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<Pozoriste> alPozoriste = new ArrayList<>();
				alPozoriste = Kontroler.getInstanca().vratiPozoriste();
				boolean postoji = false;
				for (Pozoriste p : alPozoriste) {
					if (tfNaziv.getText().toString().equalsIgnoreCase(p.getNazivPozoriste().toString())) {
						postoji = true;
						JOptionPane.showMessageDialog(null, "Pozoriste sa istim imenom je vec uneseno");
					}
				}
				if (!postoji) {
					Pozoriste novoPozoriste = new Pozoriste();
					novoPozoriste.setNazivPozoriste(tfNaziv.getText().toString());
					novoPozoriste.setAdresaPozoriste(tfAdresa.getText().toString());
					novoPozoriste.setKapacitet(Integer.valueOf(tfKapacitet.getText().toString()));
					Kontroler.getInstanca().unesiPozoriste(novoPozoriste);
				}
				osveziListu(Kontroler.getInstanca().vratiPozoriste());
			}
		});
		btnUnesi.setBounds(375, 57, 89, 23);
		contentPane.add(btnUnesi);

		table = new JTable(dtm);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 86, 584, 264);
		contentPane.add(scrollPane);

		scrollPane.setViewportView(table);

		btnIzmeni = new JButton("Izmeni");
		btnIzmeni.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Pozoriste izmeniPozoriste = new Pozoriste();
				izmeniPozoriste.setIdPozoriste(idPozoriste);
				izmeniPozoriste.setNazivPozoriste(tfNaziv.getText().toString());
				izmeniPozoriste.setAdresaPozoriste(tfAdresa.getText().toString());
				izmeniPozoriste.setKapacitet(Integer.valueOf(tfKapacitet.getText().toString()));

				Kontroler.getInstanca().izmeniPozoriste(izmeniPozoriste);
				osveziListu(Kontroler.getInstanca().vratiPozoriste());
			}
		});
		btnIzmeni.setBounds(505, 57, 89, 23);
		contentPane.add(btnIzmeni);

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				selektovaniRed = table.getSelectedRow();
				idPozoriste = (int) table.getModel().getValueAt(selektovaniRed, 0);
				tfNaziv.setText((String) table.getModel().getValueAt(selektovaniRed, 1));
				tfAdresa.setText((String) table.getModel().getValueAt(selektovaniRed, 2));
				int kap = (int) table.getModel().getValueAt(selektovaniRed, 3);
				tfKapacitet.setText(Integer.toString(kap));
			}
		});

		Object[] kolone = new Object[4];
		kolone[0] = "ID";
		kolone[1] = "Naziv pozorista";
		kolone[2] = "Adresa";
		kolone[3] = "kapacitet";

		dtm.addColumn(kolone[0]);
		dtm.addColumn(kolone[1]);
		dtm.addColumn(kolone[2]);
		dtm.addColumn(kolone[3]);
		table.getColumnModel().getColumn(0).setMaxWidth(30);
		table.getColumnModel().getColumn(1).setMinWidth(200);
		table.getColumnModel().getColumn(2).setMinWidth(200);

		alPozoriste = Kontroler.getInstanca().vratiPozoriste();
		osveziListu(alPozoriste);
	}

	private void osveziListu(ArrayList<Pozoriste> alPozoriste) {
		Object[] redovi = new Object[4];
		dtm.setRowCount(0);

		for (Pozoriste p : alPozoriste) {
			redovi[0] = p.getIdPozoriste();
			redovi[1] = p.getNazivPozoriste();
			redovi[2] = p.getAdresaPozoriste();
			redovi[3] = p.getKapacitet();
			dtm.addRow(redovi);
		}

	}
}
