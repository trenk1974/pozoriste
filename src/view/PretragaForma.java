package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import domen.Pozoriste;
import domen.Predstava;
import domen.RezervacijePredstava;

import kontroler.Kontroler;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.SwingConstants;

public class PretragaForma extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField tfPretraga;
	private JTextField tfKolicinaKarata;
	private DefaultTableModel dtm = new DefaultTableModel();
	private ArrayList<Predstava> alPredstave = new ArrayList<>();
	private ArrayList<RezervacijePredstava> alRezervacije = new ArrayList<>();
	private ArrayList<Predstava> alPredstavePozoriste = new ArrayList<>();
	private JComboBox<String> cbPozoriste;
	private MojeRezervacijeForma mrf = null;
	private int id;
	private String nazivPredstave;
	private int kapacitet;
	private String datumFormatiran;


	/**
	 * Create the frame.
	 * 
	 * @param idKorisnik
	 * @param datumFormatiran
	 */
	public PretragaForma(int idKorisnik, String datumFormatiran) {
	
		this.datumFormatiran=datumFormatiran;
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 500);
		contentPane = new JPanel();
	
		
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		table = new JTable(dtm);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 150, 974, 310);
		contentPane.add(scrollPane);


		scrollPane.setViewportView(table);

		cbPozoriste = new JComboBox<String>();
		cbPozoriste.setBounds(10, 11, 250, 20);
		contentPane.add(cbPozoriste);
		popuniCombobox();

		cbPozoriste.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String odabranoPozoriste = (String) cbPozoriste.getSelectedItem();

				osvesiTabelu(odabranoPozoriste);
			}
		});

		JLabel lblNewLabel = new JLabel("Izaberi pozoriste");
		lblNewLabel.setBounds(270, 14, 120, 14);
		contentPane.add(lblNewLabel);

		tfPretraga = new JTextField();
		tfPretraga.setBounds(10, 42, 250, 20);
		contentPane.add(tfPretraga);
		tfPretraga.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Pretraga po nazivu predstave");
		lblNewLabel_1.setBounds(270, 45, 198, 14);
		contentPane.add(lblNewLabel_1);

		tfKolicinaKarata = new JTextField();
		tfKolicinaKarata.setBounds(270, 115, 50, 20);
		contentPane.add(tfKolicinaKarata);
		tfKolicinaKarata.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("Kolicina karata");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_2.setBounds(330, 111, 120, 24);
		contentPane.add(lblNewLabel_2);

		JButton btnRezervisi = new JButton("Rezervisi");
		btnRezervisi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int novaKolicina = 0;
				try {
					novaKolicina = Integer.valueOf(tfKolicinaKarata.getText());
				} catch (NumberFormatException e1) {
					JOptionPane.showMessageDialog(null, "Unesite celi broj u polje 'Kolicina karata'");
					e1.printStackTrace();
				}
				if (novaKolicina == 0 || novaKolicina < 0) {
					JOptionPane.showMessageDialog(null, "Rezervacija neuspesna, unesite broj veci od 0");
				} else if (id == 0) {
					JOptionPane.showMessageDialog(null,
							"Morate selektovati predstavu za koju zelite rezervisati karte");
				} else if ((kapacitet - novaKolicina) < 0) {
					JOptionPane.showMessageDialog(null, "Zao nam je, nema dovoljno slobodnih karata");
				} else {
					Kontroler.getInstanca().unesiRezervaciju(id, idKorisnik, novaKolicina);
					JOptionPane.showMessageDialog(null,
							"Rezervacija uspesna za predstavu:" + nazivPredstave + ", " + novaKolicina + " karata");
				}

				osvesiTabelu("Prikazi sve");
			}
		});
		btnRezervisi.setBounds(460, 114, 89, 23);
		contentPane.add(btnRezervisi);

		JButton btnPregledRezervacija = new JButton("moje rezervacije");
		btnPregledRezervacija.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (mrf == null) {
					mrf = new MojeRezervacijeForma(idKorisnik, datumFormatiran);
				}
				mrf.setVisible(true);
			}
		});
		btnPregledRezervacija.setBounds(586, 114, 155, 23);
		contentPane.add(btnPregledRezervacija);

		JLabel lblDatum = new JLabel("Datum");
		lblDatum.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDatum.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDatum.setBounds(829, 14, 155, 17);
		contentPane.add(lblDatum);
		lblDatum.setText(datumFormatiran);

		JButton btnPretrazi = new JButton("Pretrazi");
		btnPretrazi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				pretraziPredstave();

			}
		});
		btnPretrazi.setBounds(460, 41, 89, 23);
		contentPane.add(btnPretrazi);

		JLabel lblOdabrano = new JLabel("Predstava");
		lblOdabrano.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblOdabrano.setBounds(10, 110, 250, 24);
		contentPane.add(lblOdabrano);

		JLabel lblNewLabel_3 = new JLabel("Odabrana predstava:");
		lblNewLabel_3.setBounds(10, 85, 120, 14);
		contentPane.add(lblNewLabel_3);
		
		JButton btnOsvezi = new JButton("Osvezi Listu");
		btnOsvezi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				osvesiTabelu("Prikazi sve");
				cbPozoriste.setSelectedIndex(0);
			}
		});
		btnOsvezi.setBounds(864, 114, 120, 23);
		contentPane.add(btnOsvezi);

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int red = table.getSelectedRow();
				String naziv = null;
				String kap = null;
				String idPredstave = null;
				idPredstave = (table.getModel().getValueAt(red, 0).toString());
				id = Integer.valueOf(idPredstave);
				naziv = (table.getModel().getValueAt(red, 1).toString());
				nazivPredstave = naziv;
				kap = (table.getModel().getValueAt(red, 7).toString());
				kapacitet = Integer.valueOf(kap);

				lblOdabrano.setText(nazivPredstave);

			}
		});

		Object[] kolone = new Object[9];
		kolone[0] = "ID";
		kolone[1] = "Naziv predstave";
		kolone[2] = "Zanr";
		kolone[3] = "Pozoriste";
		kolone[4] = "Autor";
		kolone[5] = "Datum";
		kolone[6] = "Vreme";
		kolone[7] = "Slobodno";
		kolone[8] = "Cena";
		dtm.addColumn(kolone[0]);
		dtm.addColumn(kolone[1]);
		dtm.addColumn(kolone[2]);
		dtm.addColumn(kolone[3]);
		dtm.addColumn(kolone[4]);
		dtm.addColumn(kolone[5]);
		dtm.addColumn(kolone[6]);
		dtm.addColumn(kolone[7]);
		dtm.addColumn(kolone[8]);

		table.getColumnModel().getColumn(0).setMaxWidth(30);
		table.getColumnModel().getColumn(1).setMinWidth(200);
		table.getColumnModel().getColumn(2).setMinWidth(75);
		table.getColumnModel().getColumn(3).setMinWidth(200);
		table.getColumnModel().getColumn(4).setMinWidth(120);

		//alPredstave = Kontroler.getInstanca().vratiPredstave(datumFormatiran);
		//alRezervacije = Kontroler.getInstanca().vratiRezervacijePredstava(datumFormatiran);
		//alPredstavePozoriste = alPredstave;
		osvesiTabelu("Prikazi sve");

	}
	

	private void pretraziPredstave() {
		String nazivPredstave = tfPretraga.getText();
		dtm.setNumRows(0);
		Object[] redovi = new Object[9];
		for (Predstava p : alPredstavePozoriste) {
			if (p.getNazivPredstava().toLowerCase().contains(nazivPredstave.toLowerCase())) {
				int slobodnaMesta = p.getKapacitet();
				redovi[0] = p.getIdPredstava();
				redovi[1] = p.getNazivPredstava();
				redovi[2] = p.getZanr();
				redovi[3] = p.getNazivPozoriste();
				redovi[4] = p.getAutor();
				redovi[5] = p.getDatumPredstave();
				redovi[6] = p.getVremePrestava();
				for (RezervacijePredstava rp : alRezervacije) {
					if (p.getIdPredstava() == rp.getIdPredstava()) {
						int rezervisano = rp.getKolicinaKarte();
						slobodnaMesta = slobodnaMesta - rezervisano;
					}
				}
				redovi[7] = slobodnaMesta;// treba odraditi izracun slobodnih mesta
				redovi[8] = p.getCena();
				dtm.addRow(redovi);
			}
		}

	}

	private void popuniCombobox() {
		ArrayList<Pozoriste> alPozoriste = new ArrayList<>();
		alPozoriste = Kontroler.getInstanca().vratiPozoriste();
		cbPozoriste.addItem("Prikazi sve");
		for (Pozoriste p : alPozoriste) {
			cbPozoriste.addItem(p.getNazivPozoriste());
		}

	}

	private void osvesiTabelu(String odabranoPozoriste) {
		
		alPredstave = Kontroler.getInstanca().vratiPredstave(datumFormatiran);
		alRezervacije = Kontroler.getInstanca().vratiRezervacijePredstava(datumFormatiran);

		Object[] redovi = new Object[9];
		dtm.setRowCount(0);
		for (Predstava p : alPredstave) {
			if (p.getNazivPozoriste().equals(odabranoPozoriste) || odabranoPozoriste.equals("Prikazi sve")) {
				int slobodnaMesta = p.getKapacitet();
				redovi[0] = p.getIdPredstava();
				redovi[1] = p.getNazivPredstava();
				redovi[2] = p.getZanr();
				redovi[3] = p.getNazivPozoriste();
				redovi[4] = p.getAutor();
				redovi[5] = p.getDatumPredstave();
				redovi[6] = p.getVremePrestava();

				for (RezervacijePredstava rp : alRezervacije) {

					if (p.getIdPredstava() == rp.getIdPredstava()) {
						int rezervisano = rp.getKolicinaKarte();
						slobodnaMesta = slobodnaMesta - rezervisano;
					}
				}
				redovi[7] = slobodnaMesta;// treba odraditi izracun slobodnih mesta
				redovi[8] = p.getCena();
				dtm.addRow(redovi);
			}

		}

	}
}
