package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import domen.RezervacijaKorisnik;
import kontroler.Kontroler;
import kontroler.SortDatum;
import kontroler.SortPredstava;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Toolkit;
import javax.swing.JComboBox;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class MojeRezervacijeForma extends JFrame {

	private JPanel contentPane;
	private String username;
	private JTable table;
	private DefaultTableModel dtm=new DefaultTableModel();
	private ArrayList<RezervacijaKorisnik> alRezervacijeKorisnik=new ArrayList<>();
	private int idRez;
	private String nazivPredstave;
	private int kolicina;
	private JTextField tfKolicina;
	private JLabel lblPredstava;
	private JLabel lblUkupnaCena;
	private JLabel lblUkupnaCena_1;
	//private DefaultTableCellRenderer dtcr=new DefaultTableCellRenderer();


	/**
	 * Create the frame.
	 * @param idKorisnik 
	 * @param datumFormatiran 
	 */
	public MojeRezervacijeForma(int idKorisnik, String datumFormatiran) {
		addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				osveziTabelu(Kontroler.getInstanca().vratiRezervacijeKorisnik(idKorisnik, datumFormatiran));
			}
		});
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Korisnik\\eclipse-workspace\\Pozoriste\\src\\slike\\drama.png"));
		setTitle("Moje Rezervacije");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 650, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDatum = new JLabel("datum");
		lblDatum.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDatum.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDatum.setBounds(534, 11, 100, 15);
		contentPane.add(lblDatum);
		lblDatum.setText(datumFormatiran);
		
		
		
		table = new JTable(dtm);
		JScrollPane scrollPane = new JScrollPane(table);		
		scrollPane.setBounds(10, 66, 624, 194);
		contentPane.add(scrollPane);		
		scrollPane.setViewportView(table);
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int red=table.getSelectedRow();
				String naziv=null;
				String kol=null;
				String idRezervacije=null;
				idRezervacije=(table.getModel().getValueAt(red, 0).toString());
				idRez=Integer.valueOf(idRezervacije);
				naziv=(table.getModel().getValueAt(red, 1).toString());
				nazivPredstave=naziv;
				kol=(table.getModel().getValueAt(red, 4).toString());
				kolicina=Integer.valueOf(kol);
				
				lblPredstava.setText(nazivPredstave);
				tfKolicina.setText(kol);

			}
		});
		JButton btnOtkazi = new JButton("Otkazi rezervaciju");
		btnOtkazi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Kontroler.getInstanca().obrisiRezervaciju(idRez);
				osveziTabelu(Kontroler.getInstanca().vratiRezervacijeKorisnik(idKorisnik,datumFormatiran));
			}
		});
		btnOtkazi.setBounds(494, 32, 140, 23);
		contentPane.add(btnOtkazi);
		
		lblPredstava = new JLabel("Predstava");
		lblPredstava.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPredstava.setBounds(10, 36, 250, 14);
		contentPane.add(lblPredstava);
		
		tfKolicina = new JTextField();
		tfKolicina.setBounds(270, 33, 49, 20);
		contentPane.add(tfKolicina);
		tfKolicina.setColumns(10);
		
		JButton btnPromeniKolicinu = new JButton("Promeni Kolicinu");
		btnPromeniKolicinu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int novaKolicina=0;
				try {
					novaKolicina=Integer.valueOf(tfKolicina.getText());
				} catch (NumberFormatException e1) {
					JOptionPane.showMessageDialog(null, "Molim vas unesite celi broj");
					e1.printStackTrace();
				}
				if(idRez==0) {
					JOptionPane.showMessageDialog(null, "Odaberite predstavu i unesite novu kolicinu karata");
				}else if(novaKolicina==0 || novaKolicina<0) {
					Kontroler.getInstanca().obrisiRezervaciju(idRez);
					JOptionPane.showMessageDialog(null, "Rezervacija obrisana");
				}else if(!proveriSlobodno(nazivPredstave, Integer.valueOf(tfKolicina.getText()), kolicina)) {
					JOptionPane.showMessageDialog(null, "Nema dovoljno slobodnih karata, izmena rezervacije nije uspela");
						
				}else {
					Kontroler.getInstanca().promeniRezervaciju(idRez,novaKolicina);
					JOptionPane.showMessageDialog(null, "Rezervacija izmenjena");
				}
				osveziTabelu(Kontroler.getInstanca().vratiRezervacijeKorisnik(idKorisnik,datumFormatiran));
				
			}
		});
		btnPromeniKolicinu.setBounds(329, 32, 140, 23);
		contentPane.add(btnPromeniKolicinu);
		
		lblUkupnaCena = new JLabel("Cena");
		lblUkupnaCena.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblUkupnaCena.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUkupnaCena.setBounds(514, 287, 120, 23);
		contentPane.add(lblUkupnaCena);
		
		lblUkupnaCena_1 = new JLabel("Ukupna Cena :");
		lblUkupnaCena_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblUkupnaCena_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUkupnaCena_1.setBounds(384, 287, 120, 23);
		contentPane.add(lblUkupnaCena_1);
		
		JLabel lblNewLabel = new JLabel("Sortiraj po :");
		lblNewLabel.setBounds(10, 271, 70, 14);
		contentPane.add(lblNewLabel);
		
		JComboBox cbSortiraj = new JComboBox();
		cbSortiraj.setBounds(85, 268, 150, 20);
		contentPane.add(cbSortiraj);
		cbSortiraj.addItem("Naziv predstave");
		cbSortiraj.addItem("Datum predstave");
		
		JButton btnOsvezi = new JButton("Osvezi listu");
		btnOsvezi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				osveziTabelu(Kontroler.getInstanca().vratiRezervacijeKorisnik(idKorisnik,datumFormatiran));
			}
		});
		btnOsvezi.setBounds(245, 267, 120, 23);
		contentPane.add(btnOsvezi);
		cbSortiraj.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				List<RezervacijaKorisnik> listRezervacija=new ArrayList<>();
				listRezervacija=Kontroler.getInstanca().vratiRezervacijeKorisnik(idKorisnik,datumFormatiran);
				String sortiranje=(String) cbSortiraj.getSelectedItem();
				if(sortiranje.equals("Naziv predstave")) {
					Collections.sort(listRezervacija, new SortPredstava());
				}else {
					Collections.sort(listRezervacija, new SortDatum());
				}
				alRezervacijeKorisnik=(ArrayList<RezervacijaKorisnik>) listRezervacija;
				osveziTabelu(alRezervacijeKorisnik);
			}
		});
		
		
		Object[] kolone=new Object[6];
		kolone[0]="ID";
		kolone[1]="Naziv predstave";
		kolone[2]="Pozoriste";
		kolone[3]="Datum";
		kolone[4]="kolicina";
		kolone[5]="ukupna cena";
		
		dtm.addColumn(kolone[0]);
		dtm.addColumn(kolone[1]);
		dtm.addColumn(kolone[2]);
		dtm.addColumn(kolone[3]);
		dtm.addColumn(kolone[4]);
		dtm.addColumn(kolone[5]);
		
		//table.setDefaultRenderer(String.class, dtcr);
		

		table.getColumnModel().getColumn(0).setMaxWidth(30);
		
		table.getColumnModel().getColumn(1).setMinWidth(150);
		table.getColumnModel().getColumn(2).setMinWidth(200);
		table.getColumnModel().getColumn(3).setMinWidth(70);
		table.getColumnModel().getColumn(4).setMinWidth(70);
		
		alRezervacijeKorisnik=Kontroler.getInstanca().vratiRezervacijeKorisnik(idKorisnik,datumFormatiran);
		
		osveziTabelu(alRezervacijeKorisnik);
		
	}


	protected boolean proveriSlobodno(String nazivPredstave2, int novaKol, int staraKolicina) {
		int idPredstava=0;
		String nazivPozorista=null;
		boolean slobodno=false;
		for(RezervacijaKorisnik rk:alRezervacijeKorisnik) {
			if(rk.getNazivPredstava().equals(nazivPredstave2)) {
				idPredstava=rk.getIdPredstava();
				nazivPozorista=rk.getNazivPozoriste();
			}
		}
		slobodno=Kontroler.getInstanca().proveriKolicinuKarata(idPredstava, nazivPozorista, novaKol, staraKolicina);
		return slobodno;
	}
	
	private void osveziTabelu(ArrayList<RezervacijaKorisnik> alRezervacijeKorisnik) {
		double totalCena=0;
		Object[] redovi=new Object[6];
		dtm.setRowCount(0);
		for(RezervacijaKorisnik rk: alRezervacijeKorisnik) {
			redovi[0]=rk.getIdRezervacije();
			redovi[1]=rk.getNazivPredstava();
			redovi[2]=rk.getNazivPozoriste();
			redovi[3]=rk.getDatumPredstave();
			redovi[4]=rk.getRezervisanoKarata();
			double ukupnaCena=rk.getRezervisanoKarata()*rk.getCenaKarte();
			redovi[5]=ukupnaCena;
			username=rk.getUsername();
			dtm.addRow(redovi);
			totalCena=totalCena+ukupnaCena;
		}
		String totalCenatxt=Double.toString(totalCena);
		totalCenatxt=totalCena+" RSD.";
		lblUkupnaCena.setText(totalCenatxt);
		
	}
}
