package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import kontroler.Kontroler;

import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import javax.swing.JCheckBox;

public class LoginForm extends JFrame {

	private JPanel contentPane;
	private JTextField tfUser;
	private JPasswordField pfPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginForm frame = new LoginForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginForm() {
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\Korisnik\\eclipse-workspace\\Pozoriste\\src\\slike\\drama.png"));
		setTitle("Pozorista Beograd - Login");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 355, 220);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("username:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(10, 11, 70, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("password:");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setBounds(10, 50, 70, 14);
		contentPane.add(lblNewLabel_1);

		tfUser = new JTextField();
		tfUser.setBounds(90, 8, 120, 20);
		contentPane.add(tfUser);
		tfUser.setColumns(10);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String username = null;

				username = tfUser.getText().toString();
				String password = new String(pfPassword.getPassword());

				if (!Kontroler.getInstanca().proveriUsername(username)) {
					JOptionPane.showMessageDialog(null, "username nije registrovan");
				} else if (!Kontroler.getInstanca().proveriPassword(username, password)) {
					JOptionPane.showMessageDialog(null, "Password nije ispravan");
				} else {
					GlavnaSwitchForma gsf = new GlavnaSwitchForma(username);
					gsf.setVisible(true);
					dispose();
				}

			}
		});
		btnLogin.setBounds(250, 7, 89, 23);
		contentPane.add(btnLogin);

		JButton btnRegistracija = new JButton("Registruj se");
		btnRegistracija.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RegistracijaForma rf = new RegistracijaForma();
				rf.setVisible(true);
			}
		});
		btnRegistracija.setBounds(209, 157, 130, 23);
		contentPane.add(btnRegistracija);

		JLabel lblNewLabel_2 = new JLabel("Registracija novih korisnika:");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_2.setBounds(25, 161, 174, 14);
		contentPane.add(lblNewLabel_2);

		pfPassword = new JPasswordField();
		pfPassword.setBounds(90, 47, 120, 20);
		contentPane.add(pfPassword);

		JCheckBox cbPrikaziPass = new JCheckBox("Prikazi password");
		cbPrikaziPass.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				if (e.getStateChange() == ItemEvent.SELECTED) {
					pfPassword.setEchoChar((char) 0);
				} else {
					pfPassword.setEchoChar('*');
				}
			}
		});

		cbPrikaziPass.setBounds(216, 46, 127, 23);
		contentPane.add(cbPrikaziPass);
	}
}
