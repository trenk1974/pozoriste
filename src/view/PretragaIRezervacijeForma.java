package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import domen.Pozoriste;
import domen.Predstava;
import domen.RezervacijaKorisnik;
import domen.RezervacijePredstava;
import kontroler.Kontroler;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class PretragaIRezervacijeForma extends JFrame {

	private JPanel contentPane;
	private JTextField tfPretraga;
	private JTextField tfKolicina;
	private JTable tablePredstave;
	private JTable tableMojeR;
	private JTextField tfNovaKolicina;
	private DefaultTableModel dtmP= new DefaultTableModel();
	private DefaultTableModel dtmR= new DefaultTableModel();
	private ArrayList<Predstava> alPredstave= new ArrayList<>();
	private ArrayList<RezervacijePredstava> alRezervacije=new ArrayList<>();
	private String datum;
	private JComboBox<String> cbPozoriste;
	private JLabel lblUkupnaCena;
	private int idKorisnik;
	private JLabel lblOdabranaPredstava, lblPozoristeOdabrano, lblDatumOdabrano;
	private int id, kapacitet, idRez, kolicinaRezervisano;
	private String nazivPredstave, nazivRezervacije;
	private JLabel lblIzabranaRez;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 * @param datumFormatiran 
	 * @param idKorisnik 
	 */
	public PretragaIRezervacijeForma(int idKorisnik, String datumFormatiran) {
		setResizable(false); //u konacnoj verziji obrisati main i dodati idKorisnik i datumFormatiran kao ulazne parametre
		this.idKorisnik=idKorisnik;
		this.datum=datumFormatiran;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		cbPozoriste = new JComboBox();
		cbPozoriste.setBounds(10, 11, 200, 20);
		contentPane.add(cbPozoriste);
		popuniCombobox();
		
		cbPozoriste.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String odabranoPozoriste = (String) cbPozoriste.getSelectedItem();

				osveziPredstave(odabranoPozoriste);
			}
		});
		
		tfPretraga = new JTextField();
		tfPretraga.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				System.out.println("TF Focus Gaind");
				alPredstave = Kontroler.getInstanca().vratiPredstave(datum);
				alRezervacije = Kontroler.getInstanca().vratiRezervacijePredstava(datum);
			}
		});
		tfPretraga.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				pretraziPredstave();
			}
		});

		tfPretraga.setBounds(10, 42, 200, 20);
		contentPane.add(tfPretraga);
		tfPretraga.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Odabrana predstava:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel.setBounds(10, 73, 140, 14);
		contentPane.add(lblNewLabel);
		
		lblOdabranaPredstava = new JLabel("Predstava");
		lblOdabranaPredstava.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblOdabranaPredstava.setBounds(10, 98, 300, 14);
		contentPane.add(lblOdabranaPredstava);
		
		JLabel lblNewLabel_1 = new JLabel("Izaberi pozoriste");
		lblNewLabel_1.setBounds(220, 14, 150, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Pretraga predstava");
		lblNewLabel_2.setBounds(220, 45, 150, 14);
		contentPane.add(lblNewLabel_2);
		
		JButton btnRezervisi = new JButton("Rezervisi");
		btnRezervisi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int novaKolicina = 0;
				try {
					novaKolicina = Integer.valueOf(tfKolicina.getText());
				} catch (NumberFormatException e1) {
					JOptionPane.showMessageDialog(null, "Unesite celi broj u polje 'Kolicina karata'");
					e1.printStackTrace();
				}
				if (novaKolicina == 0 || novaKolicina < 0) {
					JOptionPane.showMessageDialog(null, "Rezervacija neuspesna, unesite broj veci od 0");
				} else if (id == 0) {
					JOptionPane.showMessageDialog(null,
							"Morate selektovati predstavu za koju zelite rezervisati karte");
				} else if ((kapacitet - novaKolicina) < 0) {
					JOptionPane.showMessageDialog(null, "Zao nam je, nema dovoljno slobodnih karata");
				} else {
					Kontroler.getInstanca().unesiRezervaciju(id, idKorisnik, novaKolicina);
					JOptionPane.showMessageDialog(null,
							"Rezervacija uspesna za predstavu:" + nazivPredstave + ", " + novaKolicina + " karata");
				}

				osveziPredstave("Prikazi sve");
				osveziRezervacije();
				
			}
		});
		btnRezervisi.setBounds(281, 147, 89, 23);
		contentPane.add(btnRezervisi);
		
		tfKolicina = new JTextField();
		tfKolicina.setBounds(220, 148, 51, 20);
		contentPane.add(tfKolicina);
		tfKolicina.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Rezervisi karte:");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_3.setBounds(220, 123, 120, 14);
		contentPane.add(lblNewLabel_3);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(380, 0, 614, 185);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel_4 = new JLabel("Moje rezervacije: ");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel_4.setBounds(10, 11, 107, 14);
		panel.add(lblNewLabel_4);
		
		JLabel lblDatum = new JLabel(datumFormatiran);
		lblDatum.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblDatum.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDatum.setBounds(529, 12, 75, 14);
		panel.add(lblDatum);
		
		tableMojeR = new JTable(dtmR);
		JScrollPane scrollPaneRezervacije = new JScrollPane();
		
		scrollPaneRezervacije.setBounds(10, 36, 594, 114);
		panel.add(scrollPaneRezervacije);
				
		scrollPaneRezervacije.setViewportView(tableMojeR);
		
		JButton btnOtkazi = new JButton("Otkazi");
		btnOtkazi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Kontroler.getInstanca().obrisiRezervaciju(idRez);
				osveziPredstave("Prikazi sve");
				osveziRezervacije();
			}
		});
		btnOtkazi.setBounds(430, 8, 89, 23);
		panel.add(btnOtkazi);
		
		JButton btnIzmeni = new JButton("Izmeni");
		btnIzmeni.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int novaKolicina=0;
				try {
					novaKolicina=Integer.valueOf(tfNovaKolicina.getText());
				} catch (NumberFormatException e1) {
					JOptionPane.showMessageDialog(null, "Molim vas unesite celi broj");
					e1.printStackTrace();
				}
				if(idRez==0) {
					JOptionPane.showMessageDialog(null, "Odaberite predstavu i unesite novu kolicinu karata");
				}else if(novaKolicina==0 || novaKolicina<0) {
					Kontroler.getInstanca().obrisiRezervaciju(idRez);
					JOptionPane.showMessageDialog(null, "Rezervacija obrisana");
				}else if(!proveriSlobodno(nazivRezervacije, Integer.valueOf(tfNovaKolicina.getText()), kolicinaRezervisano)) {
					JOptionPane.showMessageDialog(null, "Nema dovoljno slobodnih karata, izmena rezervacije nije uspela");
						
				}else {
					Kontroler.getInstanca().promeniRezervaciju(idRez,novaKolicina);
					JOptionPane.showMessageDialog(null, "Rezervacija izmenjena");
				}
				osveziRezervacije();
				osveziPredstave("Prikazi sve");
				
				
			}
		});
		btnIzmeni.setBounds(331, 8, 89, 23);
		panel.add(btnIzmeni);
		
		tfNovaKolicina = new JTextField();
		tfNovaKolicina.setBounds(271, 9, 50, 20);
		panel.add(tfNovaKolicina);
		tfNovaKolicina.setColumns(10);
		
		lblIzabranaRez = new JLabel("New label");
		lblIzabranaRez.setHorizontalAlignment(SwingConstants.LEFT);
		lblIzabranaRez.setBounds(118, 12, 143, 14);
		panel.add(lblIzabranaRez);
		
		JLabel lblNewLabel_5 = new JLabel("Ukupna Cena:");
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_5.setBounds(382, 154, 105, 20);
		panel.add(lblNewLabel_5);
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		lblUkupnaCena = new JLabel("New label");
		lblUkupnaCena.setBounds(497, 153, 107, 20);
		panel.add(lblUkupnaCena);
		lblUkupnaCena.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUkupnaCena.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		tablePredstave = new JTable(dtmP);
		JScrollPane scrollPanePredstave = new JScrollPane();
		
		scrollPanePredstave.setBounds(10, 196, 974, 264);
		contentPane.add(scrollPanePredstave);
				
		scrollPanePredstave.setViewportView(tablePredstave);
		
		lblPozoristeOdabrano = new JLabel("Pozoriste");
		lblPozoristeOdabrano.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPozoristeOdabrano.setBounds(10, 123, 200, 14);
		contentPane.add(lblPozoristeOdabrano);
		
		lblDatumOdabrano = new JLabel("Datum");
		lblDatumOdabrano.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblDatumOdabrano.setBounds(10, 150, 100, 14);
		contentPane.add(lblDatumOdabrano);
		
		tablePredstave.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int red = tablePredstave.getSelectedRow();
				String naziv = null;
				String kap = null;
				String idPredstave = null;
				String pozoriste=null;
				String datum1=null;
				idPredstave = (tablePredstave.getModel().getValueAt(red, 0).toString());
				id = Integer.valueOf(idPredstave);
				naziv = (tablePredstave.getModel().getValueAt(red, 1).toString());
				nazivPredstave = naziv;
				kap = (tablePredstave.getModel().getValueAt(red, 7).toString());
				kapacitet = Integer.valueOf(kap);
				pozoriste = (tablePredstave.getModel().getValueAt(red, 3).toString());
				datum1 = (tablePredstave.getModel().getValueAt(red, 5).toString());

				lblOdabranaPredstava.setText(nazivPredstave);
				lblPozoristeOdabrano.setText(pozoriste);
				lblDatumOdabrano.setText(datum1);

			}
		});
		tableMojeR.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int red=tableMojeR.getSelectedRow();
				String naziv=null;
				String kol=null;
				String idRezervacije=null;
				idRezervacije=(tableMojeR.getModel().getValueAt(red, 0).toString());
				idRez=Integer.valueOf(idRezervacije);
				naziv=(tableMojeR.getModel().getValueAt(red, 1).toString());
				nazivRezervacije=naziv;
				kol=(tableMojeR.getModel().getValueAt(red, 4).toString());
				kolicinaRezervisano=Integer.valueOf(kol);
				
				lblIzabranaRez.setText(nazivRezervacije);
				tfNovaKolicina.setText(kol);
			}
		});
		
		
		Object[] kolone = new Object[9];
		kolone[0] = "ID";
		kolone[1] = "Naziv predstave";
		kolone[2] = "Zanr";
		kolone[3] = "Pozoriste";
		kolone[4] = "Autor";
		kolone[5] = "Datum";
		kolone[6] = "Vreme";
		kolone[7] = "Slobodno";
		kolone[8] = "Cena";
		dtmP.addColumn(kolone[0]);
		dtmP.addColumn(kolone[1]);
		dtmP.addColumn(kolone[2]);
		dtmP.addColumn(kolone[3]);
		dtmP.addColumn(kolone[4]);
		dtmP.addColumn(kolone[5]);
		dtmP.addColumn(kolone[6]);
		dtmP.addColumn(kolone[7]);
		dtmP.addColumn(kolone[8]);

		tablePredstave.getColumnModel().getColumn(0).setMaxWidth(30);
		tablePredstave.getColumnModel().getColumn(1).setMinWidth(200);
		tablePredstave.getColumnModel().getColumn(2).setMinWidth(75);
		tablePredstave.getColumnModel().getColumn(3).setMinWidth(200);
		tablePredstave.getColumnModel().getColumn(4).setMinWidth(120);
		
		osveziPredstave("Prikazi sve");
		
		Object[] koloneRez=new Object[6];
		koloneRez[0]="ID";
		koloneRez[1]="Naziv predstave";
		koloneRez[2]="Pozoriste";
		koloneRez[3]="Datum";
		koloneRez[4]="Kolicina";
		koloneRez[5]="Ukupna cena";
		
		dtmR.addColumn(koloneRez[0]);
		dtmR.addColumn(koloneRez[1]);
		dtmR.addColumn(koloneRez[2]);
		dtmR.addColumn(koloneRez[3]);
		dtmR.addColumn(koloneRez[4]);
		dtmR.addColumn(koloneRez[5]);
		
		tableMojeR.getColumnModel().getColumn(0).setMaxWidth(30);		
		tableMojeR.getColumnModel().getColumn(1).setMinWidth(150);
		tableMojeR.getColumnModel().getColumn(2).setMinWidth(200);
		tableMojeR.getColumnModel().getColumn(3).setMinWidth(70);
		tableMojeR.getColumnModel().getColumn(4).setMaxWidth(50);
		
		osveziRezervacije();
	}
	
	protected boolean proveriSlobodno(String nazivRezervacije2, int novaKolicina, int kolicinaRezervisano2) {
		int idPredstava=0;
		String nazivPozorista=null;
		boolean slobodno=false;
		ArrayList<RezervacijaKorisnik> alRezervacijeKorisnik =Kontroler.getInstanca().vratiRezervacijeKorisnik(idKorisnik, datum);
		for(RezervacijaKorisnik rk:alRezervacijeKorisnik) {
			if(rk.getNazivPredstava().equals(nazivRezervacije2)) {
				idPredstava=rk.getIdPredstava();
				nazivPozorista=rk.getNazivPozoriste();
			}
		}
		slobodno=Kontroler.getInstanca().proveriKolicinuKarata(idPredstava, nazivPozorista, novaKolicina, kolicinaRezervisano2);
		return slobodno;
	}

	private void osveziRezervacije() {
		double totalCena=0;
		ArrayList<RezervacijaKorisnik> alRezervacijeKorisnik=Kontroler.getInstanca().vratiRezervacijeKorisnik(idKorisnik, datum);
		Object[] redovi=new Object[6];
		dtmR.setRowCount(0);
		for(RezervacijaKorisnik rk: alRezervacijeKorisnik) {
			redovi[0]=rk.getIdRezervacije();
			redovi[1]=rk.getNazivPredstava();
			redovi[2]=rk.getNazivPozoriste();
			redovi[3]=rk.getDatumPredstave();
			redovi[4]=rk.getRezervisanoKarata();
			double ukupnaCena=rk.getRezervisanoKarata()*rk.getCenaKarte();
			redovi[5]=ukupnaCena;
			//username=rk.getUsername();
			dtmR.addRow(redovi);
			totalCena=totalCena+ukupnaCena;
		}
		String totalCenatxt=Double.toString(totalCena);
		totalCenatxt=totalCena+" RSD.";
		lblUkupnaCena.setText(totalCenatxt);
		
	}

	private void popuniCombobox() {
		ArrayList<Pozoriste> alPozoriste = new ArrayList<>();
		alPozoriste = Kontroler.getInstanca().vratiPozoriste();
		cbPozoriste.addItem("Prikazi sve");
		for (Pozoriste p : alPozoriste) {
			cbPozoriste.addItem(p.getNazivPozoriste());
		}

	}
	private void pretraziPredstave() {
		//alPredstave = Kontroler.getInstanca().vratiPredstave(datum);
		//alRezervacije = Kontroler.getInstanca().vratiRezervacijePredstava(datum);
		String nazivPredstave = tfPretraga.getText();
		dtmP.setNumRows(0);
		Object[] redovi = new Object[9];
		for (Predstava p : alPredstave) {
			if (p.getNazivPredstava().toLowerCase().contains(nazivPredstave.toLowerCase())) {
				int slobodnaMesta = p.getKapacitet();
				redovi[0] = p.getIdPredstava();
				redovi[1] = p.getNazivPredstava();
				redovi[2] = p.getZanr();
				redovi[3] = p.getNazivPozoriste();
				redovi[4] = p.getAutor();
				redovi[5] = p.getDatumPredstave();
				redovi[6] = p.getVremePrestava();

				for (RezervacijePredstava rp : alRezervacije) {

					if (p.getIdPredstava() == rp.getIdPredstava()) {
						int rezervisano = rp.getKolicinaKarte();
						slobodnaMesta = slobodnaMesta - rezervisano;
					}
				}
				redovi[7] = slobodnaMesta;// treba odraditi izracun slobodnih mesta
				redovi[8] = p.getCena();
				dtmP.addRow(redovi);
			
			}
		}

	}


	private void osveziPredstave(String odabranoPozoriste) {
		alPredstave = Kontroler.getInstanca().vratiPredstave(datum);
		alRezervacije = Kontroler.getInstanca().vratiRezervacijePredstava(datum);

		Object[] redovi = new Object[9];
		dtmP.setRowCount(0);
		for (Predstava p : alPredstave) {
			if (p.getNazivPozoriste().equals(odabranoPozoriste) || odabranoPozoriste.equals("Prikazi sve")) {
				int slobodnaMesta = p.getKapacitet();
				redovi[0] = p.getIdPredstava();
				redovi[1] = p.getNazivPredstava();
				redovi[2] = p.getZanr();
				redovi[3] = p.getNazivPozoriste();
				redovi[4] = p.getAutor();
				redovi[5] = p.getDatumPredstave();
				redovi[6] = p.getVremePrestava();

				for (RezervacijePredstava rp : alRezervacije) {

					if (p.getIdPredstava() == rp.getIdPredstava()) {
						int rezervisano = rp.getKolicinaKarte();
						slobodnaMesta = slobodnaMesta - rezervisano;
					}
				}
				redovi[7] = slobodnaMesta;
				redovi[8] = p.getCena();
				dtmP.addRow(redovi);
			}
		}
		
	}
}
