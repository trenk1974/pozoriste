package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import domen.Autor;
import domen.Zanr;
import kontroler.Kontroler;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class DodajAutorZanr extends JFrame {

	private JPanel contentPane;
	private JTextField tfAutor;
	private JTextField tfZanr;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DodajAutorZanr frame = new DodajAutorZanr();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DodajAutorZanr() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 185);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Autor : ");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(25, 45, 46, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Zanr :");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setBounds(25, 91, 46, 14);
		contentPane.add(lblNewLabel_1);

		tfAutor = new JTextField();
		tfAutor.setBounds(81, 42, 150, 20);
		contentPane.add(tfAutor);
		tfAutor.setColumns(10);

		tfZanr = new JTextField();
		tfZanr.setBounds(81, 88, 150, 20);
		contentPane.add(tfZanr);
		tfZanr.setColumns(10);

		JButton btnDodajAutor = new JButton("Dodaj Autor");
		btnDodajAutor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<Autor> alAutor = new ArrayList<>();
				boolean postoji = false;
				alAutor=Kontroler.getInstanca().vratiAutor();
				if (tfAutor.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Unesite Autora u odgovarajuce polje");
				} else {
					for (Autor a : alAutor) {
						if (a.getImeAutor().equalsIgnoreCase(tfAutor.getText().toString())) {
							postoji = true;
						}
					}
					if(postoji) {
						JOptionPane.showMessageDialog(null, "Autor "+tfAutor.getText().toString()+" vec unesen");
						tfAutor.setText("");
					}else {
						Kontroler.getInstanca().unesiAutor(tfAutor.getText().toString());
						JOptionPane.showMessageDialog(null, "Novi Autor: "+tfAutor.getText().toString()+" uspesno unesen");
						tfAutor.setText("");
					}
				}
			}
		});
		btnDodajAutor.setBounds(241, 41, 125, 23);
		contentPane.add(btnDodajAutor);

		JButton btnDodajZanr = new JButton("Dodaj Zanr");
		btnDodajZanr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<Zanr> alZanr = new ArrayList<>();
				boolean postoji = false;
				alZanr=Kontroler.getInstanca().vratiZanr();
				if (tfZanr.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Unesite Zanr u odgovarajuce polje");
				} else {
					for (Zanr z : alZanr) {
						if (z.getZanr().equalsIgnoreCase(tfZanr.getText().toString())) {
							postoji = true;
						}
					}
					if(postoji) {
						JOptionPane.showMessageDialog(null, "Zanr "+tfZanr.getText().toString()+" vec unesen");
						tfZanr.setText("");
					}else {
						Kontroler.getInstanca().unesiZanr(tfZanr.getText().toString());
						JOptionPane.showMessageDialog(null, "Novi Zanr: "+tfZanr.getText().toString()+" uspesno unesen");
						tfZanr.setText("");
					}
				}
				
			}
		});
		btnDodajZanr.setBounds(241, 87, 125, 23);
		contentPane.add(btnDodajZanr);

	}
}
