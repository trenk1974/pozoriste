package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import domen.Autor;
import domen.Pozoriste;
import domen.Predstava;
import domen.RezervacijePredstava;
import domen.Zanr;
import kontroler.Kontroler;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UnosPredstave extends JFrame {

	private JPanel contentPane;
	private JTextField tfNaziv;
	private JTextField tfCena;
	private JTable table;
	private JTextField tfDatum;
	private JTextField tfVreme;
	private DefaultTableModel dtm=new DefaultTableModel();
	private ArrayList<Predstava> alPredstave=new ArrayList<>();
	private JComboBox<String> cbPozoriste, cbZanr, cbAutor;
	private int idPredstave;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 */
	public UnosPredstave(String datumFormatiran) {
		setTitle("Unos predstava");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 900, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Naziv predstave:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(40, 11, 120, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblPozoriste = new JLabel("Pozoriste:");
		lblPozoriste.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPozoriste.setBounds(40, 36, 120, 14);
		contentPane.add(lblPozoriste);
		
		tfNaziv = new JTextField();
		tfNaziv.setBounds(170, 8, 250, 20);
		contentPane.add(tfNaziv);
		tfNaziv.setColumns(10);
		
		cbPozoriste = new JComboBox<String>();
		cbPozoriste.setBounds(170, 33, 250, 20);
		contentPane.add(cbPozoriste);
		
		cbAutor = new JComboBox<String>();
		cbAutor.setBounds(486, 8, 150, 20);
		contentPane.add(cbAutor);
		
		cbZanr = new JComboBox<String>();
		cbZanr.setBounds(486, 33, 150, 20);
		contentPane.add(cbZanr);
		
		popuniCBPozoriste();
		popuniCBAutor();
		popuniCBZanr();
		
	
		JLabel lblNewLabel_1 = new JLabel("Datum (yyyy-MM-dd):");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setBounds(10, 61, 150, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblVreme = new JLabel("Vreme (hh:mm):");
		lblVreme.setHorizontalAlignment(SwingConstants.RIGHT);
		lblVreme.setBounds(40, 86, 120, 14);
		contentPane.add(lblVreme);
		
		JLabel lblNewLabel_2 = new JLabel("Autor:");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_2.setBounds(430, 11, 46, 14);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblZanr = new JLabel("Zanr:");
		lblZanr.setHorizontalAlignment(SwingConstants.RIGHT);
		lblZanr.setBounds(430, 36, 46, 14);
		contentPane.add(lblZanr);
		
		JLabel lblCena = new JLabel("Cena:");
		lblCena.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCena.setBounds(430, 61, 46, 14);
		contentPane.add(lblCena);
				
		tfCena = new JTextField();
		tfCena.setBounds(486, 58, 86, 20);
		contentPane.add(tfCena);
		tfCena.setColumns(10);
		
		JButton btnObrisi = new JButton("Obrisi");
		btnObrisi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int reply = JOptionPane.showConfirmDialog(null, "Dali ste sigurni da zelite izbrisati predstavu?!", "Brisanje!", JOptionPane.YES_NO_OPTION);
		        if (reply == JOptionPane.YES_OPTION) {
		          Kontroler.getInstanca().obrisiPredstavu(idPredstave);
		          
		        }
		        
		        alPredstave=Kontroler.getInstanca().vratiPredstave(datumFormatiran);
				osveziTabelu(alPredstave);		        
			}
		});
		btnObrisi.setBackground(Color.RED);
		btnObrisi.setBounds(795, 95, 89, 23);
		contentPane.add(btnObrisi);
		
		JButton btnUnesi = new JButton("Unesi predstavu");
		btnUnesi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Predstava p=new Predstava();
				if(tfNaziv.getText().equals("")||tfDatum.getText().equals("")||tfVreme.getText().equals("")||
						cbPozoriste.getSelectedItem().equals("-")||cbAutor.getSelectedItem().equals("-")||
						cbZanr.getSelectedItem().equals("-")) {
					JOptionPane.showMessageDialog(null, "Molim Vas unesite sve podatke");
				}else {
				p.setNazivPredstava(tfNaziv.getText().toString());
				p.setNazivPozoriste(cbPozoriste.getSelectedItem().toString());
				p.setDatumString(tfDatum.getText().toString());
				p.setVremePrestava(tfVreme.getText().toString());
				p.setAutor(cbAutor.getSelectedItem().toString());
				p.setZanr(cbZanr.getSelectedItem().toString());
				p.setCena(Double.valueOf(tfCena.getText()));
				Kontroler.getInstanca().unesiPredstavu(p);
				
				JOptionPane.showMessageDialog(null, "Predstava uspesno unesena u bazu");
				
				tfNaziv.setText("");
				tfDatum.setText("");
				tfCena.setText("");
				tfVreme.setText("");
				cbPozoriste.setSelectedIndex(0);
				cbAutor.setSelectedIndex(0);
				cbZanr.setSelectedIndex(0);
				
				}
			}
		});
		btnUnesi.setBackground(Color.GREEN);
		btnUnesi.setBounds(646, 95, 120, 23);
		contentPane.add(btnUnesi);
		
		table = new JTable(dtm);
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 129, 874, 331);
		contentPane.add(scrollPane);
				
		scrollPane.setViewportView(table);
		
		
		tfDatum = new JTextField();
		tfDatum.setHorizontalAlignment(SwingConstants.RIGHT);
		tfDatum.setBounds(170, 58, 120, 20);
		contentPane.add(tfDatum);
		tfDatum.setColumns(10);
		
		tfVreme = new JTextField();
		tfVreme.setHorizontalAlignment(SwingConstants.RIGHT);
		tfVreme.setColumns(10);
		tfVreme.setBounds(170, 83, 120, 20);
		contentPane.add(tfVreme);
				
		JButton btnDodajAutor = new JButton("Dodaj Autor");
		btnDodajAutor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DodajAutorZanr daz=new DodajAutorZanr();
				daz.setVisible(true);
			}
		});
		btnDodajAutor.setBounds(646, 7, 120, 23);
		contentPane.add(btnDodajAutor);
		
		JButton btnDodajZanr = new JButton("Dodaj Zanr");
		btnDodajZanr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DodajAutorZanr daz=new DodajAutorZanr();
				daz.setVisible(true);
			}
		});
		btnDodajZanr.setBounds(646, 32, 120, 23);
		contentPane.add(btnDodajZanr);
		
		JButton btnDodajPozoriste = new JButton("Dodaj Pozoriste");
		btnDodajPozoriste.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UnosPozorista upo=new UnosPozorista();
				upo.setVisible(true);
			}
		});
		btnDodajPozoriste.setBounds(646, 57, 120, 23);
		contentPane.add(btnDodajPozoriste);
		
		JButton btnOsveziCB = new JButton("Osvezi CB");
		btnOsveziCB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				popuniCBAutor();
				popuniCBPozoriste();
				popuniCBZanr();
				osveziTabelu(Kontroler.getInstanca().vratiPredstave(datumFormatiran));
			}
		});
		btnOsveziCB.setBounds(795, 7, 89, 23);
		contentPane.add(btnOsveziCB);
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int red=table.getSelectedRow();								
				idPredstave = Integer.valueOf((table.getModel().getValueAt(red, 0).toString()));
				
			}
		});
		
		Object[] kolone = new Object[8];
		kolone[0] = "ID";
		kolone[1] = "Naziv predstave";
		kolone[2] = "Pozoriste";
		kolone[3] = "Datum";
		kolone[4] = "Vreme";
		kolone[5] = "Autor";
		kolone[6] = "Zanr";
		kolone[7] = "Cena";		
		
		dtm.addColumn(kolone[0]);
		dtm.addColumn(kolone[1]);
		dtm.addColumn(kolone[2]);
		dtm.addColumn(kolone[3]);
		dtm.addColumn(kolone[4]);
		dtm.addColumn(kolone[5]);
		dtm.addColumn(kolone[6]);
		dtm.addColumn(kolone[7]);

		table.getColumnModel().getColumn(0).setMaxWidth(30);
		table.getColumnModel().getColumn(1).setMinWidth(200);
		table.getColumnModel().getColumn(3).setMinWidth(100);
		table.getColumnModel().getColumn(2).setMinWidth(200);
		table.getColumnModel().getColumn(4).setMaxWidth(50);
		table.getColumnModel().getColumn(5).setMinWidth(120);
		
		alPredstave=Kontroler.getInstanca().vratiPredstave(datumFormatiran);
		osveziTabelu(alPredstave);
	}

	private void popuniCBZanr() {
		ArrayList<Zanr> alZanr=new ArrayList<>();
		alZanr= Kontroler.getInstanca().vratiZanr();
		cbZanr.removeAllItems();
		cbZanr.addItem("-");
		for(Zanr z:alZanr) {
			cbZanr.addItem(z.getZanr());
		}
		
	}

	private void popuniCBAutor() {
		ArrayList<Autor> alAutor=new ArrayList<>();
		alAutor= Kontroler.getInstanca().vratiAutor();
		cbAutor.removeAllItems();
		cbAutor.addItem("-");
		for(Autor a:alAutor) {
			cbAutor.addItem(a.getImeAutor());
		}
		
	}

	private void popuniCBPozoriste() {
		ArrayList<Pozoriste> alPozoriste = new ArrayList<>();
		alPozoriste = Kontroler.getInstanca().vratiPozoriste();
		cbPozoriste.removeAllItems();
		cbPozoriste.addItem("-");
		for (Pozoriste p : alPozoriste) {
			cbPozoriste.addItem(p.getNazivPozoriste());
		}
		
	}

	private void osveziTabelu(ArrayList<Predstava> alPredstave2) {

		Object[] redovi = new Object[8];
		dtm.setRowCount(0);
		for (Predstava p : alPredstave2) {
		
				redovi[0] = p.getIdPredstava();
				redovi[1] = p.getNazivPredstava();
				redovi[2] = p.getNazivPozoriste();
				redovi[3] = p.getDatumPredstave();
				redovi[4] = p.getVremePrestava();
				redovi[5] = p.getAutor();
				redovi[6] = p.getZanr();
				redovi[7] = p.getCena();
				dtm.addRow(redovi);
			

		}
		
	}
}
