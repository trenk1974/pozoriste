package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import domen.Korisnik;
import kontroler.Kontroler;

import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class GlavnaSwitchForma extends JFrame {

	private JPanel contentPane;
	private MojeRezervacijeForma mrf = null;
	private int idKorisnik;
	private Calendar danasnjiDatum = Calendar.getInstance();
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	private String datumFormatiran;

	/**
	 * Create the frame.
	 * 
	 * @param username
	 */
	public GlavnaSwitchForma(String username) {

		setTitle("Pozorista Beograd - Main Menu");
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\Korisnik\\eclipse-workspace\\Pozoriste\\src\\slike\\drama.png"));
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 380, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblDobroDosli = new JLabel("New label");
		lblDobroDosli.setBounds(10, 11, 194, 14);
		contentPane.add(lblDobroDosli);
		Korisnik korisnik = Kontroler.getInstanca().vratiKorisnik(username);
		lblDobroDosli.setText("Dobro dosli " + korisnik.getIme());

		JButton btnPretraga = new JButton("Pretraga i rezervacija");
		btnPretraga.setBounds(10, 58, 160, 23);
		contentPane.add(btnPretraga);
		idKorisnik = korisnik.getIdKorisnik();

		JLabel lblDatum = new JLabel("datum");
		lblDatum.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDatum.setBounds(278, 11, 86, 14);
		contentPane.add(lblDatum);
		datumFormatiran = (String) sdf.format(danasnjiDatum.getTime());
		lblDatum.setText(datumFormatiran);
		
		JButton btnUnosPozorista = new JButton("Unos pozorista");
		btnUnosPozorista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UnosPozorista upo=new UnosPozorista();
				upo.setVisible(true);
			}
		});
		btnUnosPozorista.setBounds(10, 137, 160, 23);
		contentPane.add(btnUnosPozorista);
				
		JButton btnUnosPredstava = new JButton("Unos Predstava");
		btnUnosPredstava.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UnosPredstave upr=new UnosPredstave(datumFormatiran);
				upr.setVisible(true);
			}
		});
		btnUnosPredstava.setBounds(204, 137, 160, 23);
		contentPane.add(btnUnosPredstava);
		if(!username.equals("admin")) {
			btnUnosPozorista.setVisible(false);
			btnUnosPredstava.setVisible(false);
		}
		
		btnPretraga.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				PretragaIRezervacijeForma pf = new PretragaIRezervacijeForma(idKorisnik, datumFormatiran);
				pf.setVisible(true);
			}
		});
	}
}
