package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import kontroler.Kontroler;

import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import javax.swing.JCheckBox;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class RegistracijaForma extends JFrame {

	private JPanel contentPane;
	private JTextField tfImeKorisnik;
	private JTextField tfPrezimeKorisnik;
	private JTextField tfEmailKorisnik;
	private JTextField tfTelefonKorisnik;
	private JTextField tfUsername;
	private JPasswordField pfPassword1;
	private JPasswordField pfPassword2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegistracijaForma frame = new RegistracijaForma();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegistracijaForma() {
		setTitle("Pozorista Beograd - Registracija");
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\Korisnik\\eclipse-workspace\\Pozoriste\\src\\slike\\drama.png"));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 390, 220);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Ime:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(10, 11, 70, 14);
		contentPane.add(lblNewLabel);

		JLabel lblPrezime = new JLabel("Prezime:");
		lblPrezime.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPrezime.setBounds(10, 36, 70, 14);
		contentPane.add(lblPrezime);

		JLabel lblEmail = new JLabel("email:");
		lblEmail.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEmail.setBounds(10, 61, 70, 14);
		contentPane.add(lblEmail);

		JLabel lblTelefon = new JLabel("Telefon:");
		lblTelefon.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTelefon.setBounds(10, 86, 70, 14);
		contentPane.add(lblTelefon);

		JLabel lblUsername = new JLabel("username:");
		lblUsername.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUsername.setBounds(10, 111, 70, 14);
		contentPane.add(lblUsername);

		JLabel lblPassword = new JLabel("password:");
		lblPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPassword.setBounds(10, 136, 70, 14);
		contentPane.add(lblPassword);

		JLabel lblPasswordcheck = new JLabel("password:");
		lblPasswordcheck.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPasswordcheck.setBounds(10, 161, 70, 14);
		contentPane.add(lblPasswordcheck);

		tfImeKorisnik = new JTextField();
		tfImeKorisnik.setBounds(90, 8, 150, 20);
		contentPane.add(tfImeKorisnik);
		tfImeKorisnik.setColumns(10);

		tfPrezimeKorisnik = new JTextField();
		tfPrezimeKorisnik.setBounds(90, 33, 150, 20);
		contentPane.add(tfPrezimeKorisnik);
		tfPrezimeKorisnik.setColumns(10);

		tfEmailKorisnik = new JTextField();
		tfEmailKorisnik.setBounds(90, 58, 150, 20);
		contentPane.add(tfEmailKorisnik);
		tfEmailKorisnik.setColumns(10);

		tfTelefonKorisnik = new JTextField();
		tfTelefonKorisnik.setBounds(90, 83, 150, 20);
		contentPane.add(tfTelefonKorisnik);
		tfTelefonKorisnik.setColumns(10);

		tfUsername = new JTextField();
		tfUsername.setBounds(90, 108, 150, 20);
		contentPane.add(tfUsername);
		tfUsername.setColumns(10);

		JButton btnRegistruj = new JButton("Registruj");
		btnRegistruj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String imeKorisnik, prezimeKorisnik, emailKorisnik, telefonKorisnik, username;

				imeKorisnik = tfImeKorisnik.getText().toString();
				prezimeKorisnik = tfPrezimeKorisnik.getText().toString();
				emailKorisnik = tfEmailKorisnik.getText().toString();
				telefonKorisnik = tfTelefonKorisnik.getText().toString();
				username = tfUsername.getText().toString();
				String password1 = new String(pfPassword1.getPassword());
				String password2 = new String(pfPassword2.getPassword());
				try {
					Integer.parseInt(telefonKorisnik); // provera dali je u polju telefona unesen broj

					if (imeKorisnik.equals("") || prezimeKorisnik.equals("") || emailKorisnik.equals("") // provera dali
																											// su sva
																											// polja
																											// popunjena
							|| telefonKorisnik.equals("") || username.equals("") || password1.equals("")
							|| password2.equals("")) {
						JOptionPane.showMessageDialog(null, "Molim Vas unesite sva polja");
					} else if (!password1.equals(password2)) {
						JOptionPane.showMessageDialog(null, "Password polja se ne slazu"); // provera passworda, dali su
																							// jednaki
					} else if (!emailKorisnik.contains("@")) {
						JOptionPane.showMessageDialog(null, "Unesite validan email"); // provera email-a, dali sadrzi @
					} else if (Kontroler.getInstanca().proveriUsername(username)) {
						JOptionPane.showMessageDialog(null, "Korisnik sa tim username vec postoji");
					} else {
						Kontroler.getInstanca().unesiKorisnik(imeKorisnik, prezimeKorisnik, username, password1,
								emailKorisnik, telefonKorisnik);

						JOptionPane.showMessageDialog(null, username + ", uspesno ste se registrovali");
						dispose();// ugasi formu nakon uspesne registracije
					}
				} catch (NumberFormatException e1) {
					JOptionPane.showMessageDialog(null, "Unesite validan broj telefona");
				}
			}
		});
		btnRegistruj.setBackground(Color.GREEN);
		btnRegistruj.setBounds(280, 157, 89, 23);
		contentPane.add(btnRegistruj);

		JButton btnOdustani = new JButton("Odustani");
		btnOdustani.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnOdustani.setBackground(Color.RED);
		btnOdustani.setBounds(280, 7, 89, 23);
		contentPane.add(btnOdustani);

		pfPassword1 = new JPasswordField();
		pfPassword1.setBounds(90, 133, 150, 20);
		contentPane.add(pfPassword1);

		pfPassword2 = new JPasswordField();
		pfPassword2.setBounds(90, 158, 150, 20);
		contentPane.add(pfPassword2);

		JCheckBox cbPrikaziPass = new JCheckBox("Prikazi Password");
		cbPrikaziPass.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				if (e.getStateChange() == ItemEvent.SELECTED) {
					pfPassword1.setEchoChar((char) 0);
					pfPassword2.setEchoChar((char) 0);
				} else {
					pfPassword1.setEchoChar('*');
					pfPassword2.setEchoChar('*');
				}
			}
		});
		cbPrikaziPass.setHorizontalAlignment(SwingConstants.RIGHT);
		cbPrikaziPass.setBounds(246, 132, 131, 23);
		contentPane.add(cbPrikaziPass);
	}
}
