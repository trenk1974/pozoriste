package kontroler;

import java.util.ArrayList;

import broker.DBComm;
import domen.Autor;
import domen.Korisnik;
import domen.Pozoriste;
import domen.Predstava;
import domen.RezervacijaKorisnik;
import domen.RezervacijePredstava;
import domen.Zanr;

public class Kontroler {

	public static Kontroler instanca;
	private Korisnik korisnik;

	public static Kontroler getInstanca() {
		if (instanca == null) {
			instanca = new Kontroler();
		}
		return instanca;
	}

	public boolean proveriUsername(String username) {
		boolean postojiUsername = true;

		DBComm.getBroker().otvoriKonekciju();
		postojiUsername = DBComm.getBroker().proveriUsername(username);
		DBComm.getBroker().zatvoriKonekciju();
		return postojiUsername;
	}

	public void unesiKorisnik(String imeKorisnik, String prezimeKorisnik, String username, String password1,
			String emailKorisnik, String telefonKorisnik) {
		DBComm.getBroker().otvoriKonekciju();
		DBComm.getBroker().unesiKorisnik(imeKorisnik, prezimeKorisnik, username, password1, emailKorisnik,
				telefonKorisnik);
		DBComm.getBroker().zatvoriKonekciju();
	}

	public boolean proveriPassword(String username, String password) {
		boolean passOK = false;
		DBComm.getBroker().otvoriKonekciju();
		passOK = DBComm.getBroker().proveriPass(username, password);
		DBComm.getBroker().zatvoriKonekciju();
		return passOK;
	}

	public Korisnik vratiKorisnik(String user) {
		DBComm.getBroker().otvoriKonekciju();
		korisnik = DBComm.getBroker().vratiKorisnik(user);
		DBComm.getBroker().zatvoriKonekciju();
		return korisnik;
	}

	public ArrayList<Predstava> vratiPredstave(String datumFormatiran) {
		ArrayList<Predstava> alPredstave = new ArrayList<>();
		DBComm.getBroker().otvoriKonekciju();
		alPredstave = DBComm.getBroker().vratiPredstave(datumFormatiran);
		DBComm.getBroker().zatvoriKonekciju();
		return alPredstave;
	}

	public ArrayList<RezervacijePredstava> vratiRezervacijePredstava(String datumFormatiran) {
		ArrayList<RezervacijePredstava> alRezervacije = new ArrayList<>();
		DBComm.getBroker().otvoriKonekciju();
		alRezervacije = DBComm.getBroker().vratiRezervacijePredstava(datumFormatiran);
		DBComm.getBroker().zatvoriKonekciju();
		return alRezervacije;
	}

	public ArrayList<Pozoriste> vratiPozoriste() {
		ArrayList<Pozoriste> alPozoriste = new ArrayList<>();
		DBComm.getBroker().otvoriKonekciju();
		alPozoriste = DBComm.getBroker().vratiPozoriste();
		DBComm.getBroker().zatvoriKonekciju();
		return alPozoriste;
	}

	public ArrayList<RezervacijaKorisnik> vratiRezervacijeKorisnik(int idKorisnik, String datumFormatiran) {
		ArrayList<RezervacijaKorisnik> alRezervacijeKorisnik = new ArrayList<>();
		DBComm.getBroker().otvoriKonekciju();
		alRezervacijeKorisnik = DBComm.getBroker().vratiRezervacijeKorisnik(idKorisnik, datumFormatiran);
		DBComm.getBroker().zatvoriKonekciju();
		return alRezervacijeKorisnik;
	}

	public void obrisiRezervaciju(int idRez) {
		DBComm.getBroker().otvoriKonekciju();
		DBComm.getBroker().obrisiRezervaciju(idRez);
		DBComm.getBroker().zatvoriKonekciju();

	}

	public void promeniRezervaciju(int id, int novaKolicina) {
		DBComm.getBroker().otvoriKonekciju();
		DBComm.getBroker().promeniRezervaciju(id, novaKolicina);
		DBComm.getBroker().zatvoriKonekciju();

	}

	public void unesiRezervaciju(int id, int idKorisnik, int novaKolicina) {
		DBComm.getBroker().otvoriKonekciju();
		DBComm.getBroker().unesiRezervaciju(id, idKorisnik, novaKolicina);
		DBComm.getBroker().zatvoriKonekciju();

	}

	public void izmeniPozoriste(Pozoriste izmeniPozoriste) {
		DBComm.getBroker().otvoriKonekciju();
		DBComm.getBroker().izmeniPozoriste(izmeniPozoriste);
		DBComm.getBroker().zatvoriKonekciju();
		
	}

	public void unesiPozoriste(Pozoriste novoPozoriste) {
		DBComm.getBroker().otvoriKonekciju();
		DBComm.getBroker().unesiPozoriste(novoPozoriste);
		DBComm.getBroker().zatvoriKonekciju();
		
	}

	public ArrayList<Zanr> vratiZanr() {
		ArrayList<Zanr> alZanr=new ArrayList<>();
		DBComm.getBroker().otvoriKonekciju();
		alZanr=DBComm.getBroker().vratiZanr();
		DBComm.getBroker().zatvoriKonekciju();
		return alZanr;
	}

	public ArrayList<Autor> vratiAutor() {
		ArrayList<Autor> alAutor=new ArrayList<>();
		DBComm.getBroker().otvoriKonekciju();
		alAutor=DBComm.getBroker().vratiAutor();
		DBComm.getBroker().zatvoriKonekciju();
		return alAutor;
	}

	public void obrisiPredstavu(int idPredstave) {
		DBComm.getBroker().otvoriKonekciju();
		DBComm.getBroker().obrisiPredstavu(idPredstave);
		DBComm.getBroker().zatvoriKonekciju();
		
	}

	public void unesiPredstavu(Predstava p) {
		DBComm.getBroker().otvoriKonekciju();
		DBComm.getBroker().unesiPredstavu(p);
		DBComm.getBroker().zatvoriKonekciju();
		
	}

	public void unesiAutor(String autor) {
		DBComm.getBroker().otvoriKonekciju();
		DBComm.getBroker().unesiAutor(autor);
		DBComm.getBroker().zatvoriKonekciju();
		
	}
	public void unesiZanr(String zanr) {
		DBComm.getBroker().otvoriKonekciju();
		DBComm.getBroker().unesiZanr(zanr);
		DBComm.getBroker().zatvoriKonekciju();
		
	}
	public boolean proveriKolicinuKarata(int idPredstava, String nazivPozorista, int novaKol, int staraKolicina) {
		boolean slobodno=false;
		DBComm.getBroker().otvoriKonekciju();
		slobodno=DBComm.getBroker().proveriSlobodnuKolicinu(idPredstava, nazivPozorista, novaKol, staraKolicina);
		DBComm.getBroker().zatvoriKonekciju();
		return slobodno;
	}

}
