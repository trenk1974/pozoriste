package kontroler;

import java.util.Comparator;

import domen.RezervacijaKorisnik;

public class SortDatum implements Comparator<RezervacijaKorisnik>{

	@Override
	public int compare(RezervacijaKorisnik arg0, RezervacijaKorisnik arg1) {
		
		return arg0.getDatumPredstave().compareTo(arg1.getDatumPredstave());
	}

}
