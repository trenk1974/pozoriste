package kontroler;

import java.util.Comparator;

import domen.RezervacijaKorisnik;

public class SortPredstava implements Comparator<RezervacijaKorisnik>{

	@Override
	public int compare(RezervacijaKorisnik arg0, RezervacijaKorisnik arg1) {
		
		return arg0.getNazivPredstava().compareToIgnoreCase(arg1.getNazivPredstava());
	}

}
